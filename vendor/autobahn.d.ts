// https://github.com/crossbario/autobahn-js/blob/v0.8.2/autobahn/autobahn.js

declare namespace ab {
    const CONNECTION_CLOSED: number;
    const CONNECTION_LOST: number;
    const CONNECTION_RETRIES_EXCEEDED: number;
    const CONNECTION_UNREACHABLE: number;
    const CONNECTION_UNSUPPORTED: number;
    const CONNECTION_UNREACHABLE_SCHEDULED_RECONNECT: number;
    const CONNECTION_LOST_SCHEDULED_RECONNECT: number;

    function connect(
        wsuri: string,
        onconnect: (
            sess: Session,
        ) => void,
        onhangup?: (
            code: number,
            reason: string,
            detail?: {
                delay: number,
                retries: number,
                maxretries: number,
            },
        ) => void,
        options?: {
            retryDelay?: number,
            maxRetries?: number,
            skipSubprotocolCheck?: boolean,
            skipSubprotocolAnnounce?: boolean,
        },
    ): void;

    class Session {
        constructor(
            wsuri: string,
            onopen: () => void,
            onclose: (code: number, reason: string) => void,
            options?: {
                skipSubprotocolAnnounce?: boolean,
            },
        )

        subscribe(
            topicuri: string,
            callback: (
                topicuri: string,
                message: object,
            ) => void,
        ): void

        unsubscribe(
            topicuri: string,
            callback?: (
                topicuri: string,
                message: string,
            ) => void,
        ): void

        publish(
            topicuri: string,
            event: string,
            exclude?: string[] | boolean,
            eligible?: string[],
        ): void
    }
}
