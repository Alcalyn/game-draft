Game draft
==========

Test with Pixi.js, DragonBones, and Blender COA Tools.

Demo: <https://alcalyn.gitlab.io/game-draft/>.


## Install

``` bash
make install
```


## Run

``` bash
make dev
```

Then go to the url displayed in console.


## Development

Needs Gimp, Blender, Nodejs.

- Draw personages (Gimp, Krita...)
- Export layers (i.e OpenRaster file, `.ora`)
- Open layers with Gimp and Export to COA tools
- Animate personages with Blender (plugin COA Tools)
- Export to DragonBones sprites from Blender
- Load sprites with DragonBonesJS over pixi.js
