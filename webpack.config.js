const path = require('path');
const Dotenv = require('dotenv-webpack');

module.exports = {
  mode: 'development',
  entry: './src/app.ts',
  //entry: './src/ObjectCreatorApp.ts',
  devtool: 'inline-source-map',
  devServer: {
    disableHostCheck: true,
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        enforce: 'pre',
        use: 'tslint-loader',
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
      },
      {
        test: /\.js?$/,
        use: 'babel-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    alias: {
      // Related to tsconfig.json: compilerOptions.paths
      engine: path.join(__dirname, 'src', 'engine'),
      game: path.join(__dirname, 'src', 'game'),
      src: path.join(__dirname, 'src'),
    },
  },
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
    libraryTarget: 'global',
    library: 'Main',
  },
  plugins: [
    new Dotenv(),
  ],
};
