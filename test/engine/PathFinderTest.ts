import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import PathFinder from 'engine/PathFinder';
import Points from 'engine/Geometry/Points';
import Polygons from 'engine/Geometry/Polygons';
import RestrictedArea from 'engine/Scene/Room/RestrictedArea';

@suite
export default class PathFinderTest {

    @test
    public testPathFinderPolygonInSquareDirectPath() {
        const square: Polygon = [[
            {x: 0, y: 0},
            {x: 100, y: 0},
            {x: 100, y: 100},
            {x: 0, y: 100},
        ]];

        const start = {x: 25, y: 25};
        const end = {x: 75, y: 60};
        const path = PathFinder.pathFinderPolygon(start, end, square);

        assert.equal(path.length, 2);
        assert.deepStrictEqual(path[0], start);
        assert.deepStrictEqual(path[1], end);
    }

    @test
    public testPathFinderPolygonAvoidCorner() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 400, y: 0},
            {x: 400, y: 300},
            {x: 300, y: 300},
            {x: 200, y: 100},
            {x: 100, y: 300},
            {x: 0, y: 300},
        ]];

        const start = {x: 100, y: 200};
        const end = {x: 300, y: 200};
        const path = PathFinder.pathFinderPolygon(start, end, polygon);

        assert.equal(path.length, 3);
        assert.deepStrictEqual(path[0], start);
        assert.deepStrictEqual(path[1], {x: 200, y: 100});
        assert.deepStrictEqual(path[2], end);
    }

    @test
    public testPathFinderPolygonAvoidSquareCorner() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 400, y: 0},
            {x: 400, y: 300},
            {x: 300, y: 300},
            {x: 300, y: 100},
            {x: 100, y: 100},
            {x: 100, y: 300},
            {x: 0, y: 300},
        ]];

        const start = {x: 50, y: 250};
        const end = {x: 350, y: 250};
        const path = PathFinder.pathFinderPolygon(start, end, polygon);

        assert(path !== null, 'Pathfinder should find a solution');
        assert.equal(path.length, 4);
        assert.deepStrictEqual(path[0], start);
        assert.deepStrictEqual(path[1], {x: 100, y: 100});
        assert.deepStrictEqual(path[2], {x: 300, y: 100});
        assert.deepStrictEqual(path[3], end);
    }

    @test
    public testPathFinderPolygonZPath() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 400, y: 0},
            {x: 400, y: 100},
            {x: 100, y: 300},
            {x: 400, y: 300},
            {x: 400, y: 400},
            {x: 0, y: 400},
            {x: 0, y: 300},
            {x: 300, y: 100},
            {x: 0, y: 100},
        ]];

        const start = {x: 50, y: 50};
        const end = {x: 350, y: 350};
        const path = PathFinder.pathFinderPolygon(start, end, polygon);

        assert.equal(path.length, 4);
        assert.deepStrictEqual(path[0], start);
        assert.deepStrictEqual(path[1], {x: 300, y: 100});
        assert.deepStrictEqual(path[2], {x: 100, y: 300});
        assert.deepStrictEqual(path[3], end);
    }

    @test
    public testPathFinderPolygonOnRoundedAnglesPolygon() {
        const polygon: Polygon = [[
            {x: 740, y: 329},
            {x: 923, y: 613},
            {x: 562, y: 807},
            {x: 511, y: 706},

            // First rounded angle
            {x: 576, y: 672},
            {x: 578, y: 671},
            {x: 580, y: 669},
            {x: 581, y: 667},
            {x: 582, y: 666},
            {x: 583, y: 663},
            {x: 584, y: 661},
            {x: 583, y: 659},
            {x: 584, y: 657},
            {x: 583, y: 654},
            {x: 582, y: 652},

            // Second rounded angle
            {x: 527, y: 550},
            {x: 525, y: 548},
            {x: 524, y: 546},
            {x: 522, y: 545},
            {x: 520, y: 543},
            {x: 518, y: 543},
            {x: 516, y: 542},
            {x: 514, y: 543},
            {x: 511, y: 542},
            {x: 509, y: 543},
            {x: 507, y: 544},

            {x: 454, y: 574},
            {x: 428, y: 509},
        ]];

        [
            [{x: 539, y: 715}, {x: 455.5, y: 534}],
            [{x: 566, y: 723}, {x: 471.5, y: 525}],
        ].forEach((way: Segment) => {
            const path = PathFinder.pathFinderPolygon(way[0], way[1], polygon);
            const birdDistance = Points.distance(way);
            let distance: number = 0;
            for (let i = 1; i < path.length; i++) {
                distance += Points.distance([path[i - 1], path[i]]);
            }
            assert(
                distance < birdDistance * 2,
                `Pathfinder returns a path too long for `
                    + `(${way[0].x}, ${way[0].y}), (${way[1].x}, ${way[1].y}): `
                    + `${distance} / ${birdDistance * 2}`,
            );
        });
    }

    @test
    public testPathFinderPolygonOnPolygonWithHole() {
        const polygon: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 40, y: 40},
                {x: 40, y: 60},
                {x: 60, y: 60},
                {x: 60, y: 40},
            ],
        ];

        const start = {x: 45, y: 75};
        const end = {x: 55, y: 25};
        const path = PathFinder.pathFinderPolygon(start, end, polygon);

        assert(null !== path, 'Pathfinder should find path on polygon with hole');
        assert.equal(path.length, 4);
        assert.deepStrictEqual(path, [
            start,
            {x: 40, y: 60},
            {x: 40, y: 40},
            end,
        ]);
    }

    @test
    public testPathFinderPolygonOnPaddedPolygonWithHole() {
        const polygon = Polygons.padding([
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 40, y: 40},
                {x: 40, y: 60},
                {x: 60, y: 60},
                {x: 60, y: 40},
            ],
        ], 12);

        const start = {x: 55, y: 15};
        const end = {x: 45, y: 85};
        const path = PathFinder.pathFinderPolygon(start, end, polygon);

        assert(null !== path, 'Pathfinder should find path on polygon with hole');
        assert.equal(path.length, 5);
        assert.deepStrictEqual(path, [
            start,
            { x: 70, y: 34 },
            { x: 70, y: 66 },
            { x: 66, y: 70 },
            end,
        ]);
    }

    @test
    public testPathFinderRoom() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 400, y: 0},
            {x: 400, y: 300},
            {x: 300, y: 300},
            {x: 300, y: 100},
            {x: 100, y: 100},
            {x: 100, y: 300},
            {x: 0, y: 300},
        ]];

        const restrictedArea = new RestrictedArea(polygon, 10);

        const path = PathFinder.pathFinderRestrictedArea(
            {x: 50, y: 250},
            {x: 350, y: 250},
            restrictedArea,
        );

        assert(path, 'A path should be returned');
        assert.strictEqual(path.length, 6);
        assert.deepStrictEqual(path, [
            {x: 50, y: 250},
            {x: 91, y: 95},
            {x: 95, y: 91},
            {x: 305, y: 91},
            {x: 309, y: 95},
            {x: 350, y: 250},
        ]);
    }

    @test
    public testPathFinderRestrictedAreaWithStartAtTheLimitOfObstacle() {
        const area: Polygon = [
            [
                {x: 0, y: 0},
                {x: 400, y: 0},
                {x: 400, y: 400},
                {x: 0, y: 400},
            ],
            [
                {x: 110.8, y: 260},
                {x: 193.2, y: 330.8},
                {x: 313.20000000000005, y: 282},
                {x: 228, y: 220},
            ],
        ];

        const from: Point = {
            x: 179.08802195481817,
            y: 339.6536091420734,
        };
        const to: Point = {
            x: 247.2917156021321,
            y: 212.77748667449634,
        };

        const restrictedArea = new RestrictedArea(area, 16);
        const path = PathFinder.pathFinderRestrictedArea(from, to, restrictedArea);

        assert.notStrictEqual(path, null, 'There should have a path');
    }

    @test
    public testCreateDijkstraGraphFromPolygonSquareWithSquareHole() {
        const exteriorPoint: Point = {x: 0, y: 0};
        const holePoint: Point = {x: 40, y: 40};

        const polygon: Polygon = [
            [
                exteriorPoint,
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                holePoint,
                {x: 40, y: 60},
                {x: 60, y: 60},
                {x: 60, y: 40},
            ],
        ];

        const graph = PathFinder.createDijkstraGraphFromPolygon(polygon);

        assert.strictEqual(graph.nodes.length, 4);
        assert.ok(graph.findNodeByPayload(holePoint), 'There should have a node of a hole point');
        assert.ok(!graph.findNodeByPayload(exteriorPoint), 'There should not have node for exterior ring');
    }
}
