import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import FileSystemLoader from 'engine/Xml/XmlSerializer/FileSystemLoader';
import Scene from 'engine/Scene/Scene';
import EngineSerializer from 'engine/Xml/EngineSerializer';

@suite
export default class EngineSerializerTest {

    @test
    public test(done: (error?: string) => void) {
        const serializer = new EngineSerializer(new FileSystemLoader());

        serializer.importFile<Scene>(__dirname + '/scene.xml')
            .then(scene => {
                assert.ok(scene instanceof Scene, 'Import should return an instance of Scene');

                assert.strictEqual(scene.background.children.length, 1);
                assert.strictEqual(scene.background.getChildAt(0).scale.x, 0.4);

                assert.strictEqual(scene.actors.children.length, 2);
                assert.deepStrictEqual(scene.actors.children[0].getShape(), [{x: 0, y: 0}, {x: 604, y: -535}]);

                done();
            })
            .catch(reason => {
                done(reason);
            })
        ;
    }
}
