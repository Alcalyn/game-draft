import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import ActorContainer from 'engine/Scene/Actor/ActorContainer';
import Actor from 'engine/Scene/Actor/Actor';

@suite
export default class ActorContainerTest {

    @test
    public testSortChildrenOnePointOneActor() {
        const container = new ActorContainer();

        const character = new Actor();
        const shape = new Actor();

        container.addChild(character, shape);

        shape.setShape([
            {x: 0, y: 0},
            {x: 10, y: 0},
            {x: 20, y: -10},
        ]);

        character.position.set(5, 1);
        container.sortChildren();

        assert(
            container.getChildIndex(character) > container.getChildIndex(shape),
            'Character must be in front of shape',
        );

        character.position.set(5, -1);
        container.sortChildren();

        assert(
            container.getChildIndex(character) < container.getChildIndex(shape),
            'Character must be behind shape',
        );

        character.position.set(15, -2);
        container.sortChildren();

        assert(
            container.getChildIndex(character) > container.getChildIndex(shape),
            'Character must be in front of shape',
        );

        character.position.set(15, -7);
        container.sortChildren();

        assert(
            container.getChildIndex(character) < container.getChildIndex(shape),
            'Character must be behind shape',
        );
    }

    @test
    public testSortChildrenOnePointManyActors() {
        const container = new ActorContainer();

        const character = new Actor();
        const actors: Actor[] = [];
        let i: number;

        character.name = 'character';

        container.addChild(character);

        for (i = 0; i < 8; i++) {
            const actor = new Actor();

            actor.name = 'actor_' + i;
            actor.setShape([
                {x: -10, y: -10},
                {x: 0, y: 0},
                {x: 10, y: -10},
            ]);

            actor.position.set(i, i * 5);

            container.addChild(actor);
            actors.push(actor);
        }

        character.position.set(4 + 1, 20);
        container.sortChildren();

        assert.deepStrictEqual(container.children.map(child => child.name), [
            'actor_0',
            'actor_1',
            'actor_2',
            'actor_3',
            'actor_4',
            'character',
            'actor_5',
            'actor_6',
            'actor_7',
        ]);
    }

    @test
    public testSortChildrenOnePointManyActorsSpreadOnScene() {
        const container = new ActorContainer();

        const character = new Actor();
        const actors: Actor[] = [];
        let i: number;

        character.name = 'character';

        container.addChild(character);

        for (i = 0; i < 4; i++) {
            const actor = new Actor();

            actor.name = 'actor_' + i;
            actor.setShape([
                {x: 0, y: 0},
                {x: 10, y: 0},
            ]);

            container.addChild(actor);
            actors.push(actor);
        }

        actors[0].position.set(-1, -1);
        character.position.set(0, 0);
        actors[1].position.set(-1, 1);

        actors[2].position.set(-100, 10);
        actors[3].position.set(100, -10);

        container.sortChildren();

        assert(
            container.getChildIndex(character) > container.getChildIndex(actors[0]),
            'Character must be in front of actor 0',
        );
        assert(
            container.getChildIndex(character) < container.getChildIndex(actors[1]),
            'Character must be behind actor 1',
        );
    }

    /**
     * sortChildren must not add nor remove child to not dispatch childAdded/removed events.
     */
    @test
    public testSortChildrenDoesNotDispatchChildAddedRemovedEvents() {
        const container = new ActorContainer();

        const character = new Actor();
        const shape = new Actor();

        container.addChild(character, shape);

        shape.setShape([
            {x: 0, y: 0},
            {x: 10, y: 0},
            {x: 20, y: -10},
        ]);

        character.position.set(5, 1);

        container.on('childAdded', () => assert.fail('Container dispatched childAdded event.'));
        container.on('childRemoved', () => assert.fail('Container dispatched childRemoved event.'));

        container.sortChildren();
    }

    @test
    public testSortChildren2PointsAroundSimpleShape() {
        const container = new ActorContainer();

        const point0 = new Actor();
        const point1 = new Actor();

        point0.name = 'point0';
        point1.name = 'point1';

        container.addChild(point1, point0);

        point0.position.set(2, 3);
        point1.position.set(8, 5);

        container.sortChildren();

        assert.deepStrictEqual(container.children.map(actor => actor.name).join(), 'point0,point1');

        const shape = new Actor();

        shape.name = 'shape';

        shape.setShape([
            {x: 0, y: 0},
            {x: 10, y: 10},
        ]);

        container.addChild(shape);

        container.sortChildren();

        // Make sure point1 is below point0 because of shape
        assert.deepStrictEqual(container.children.map(actor => actor.name).join(), 'point1,shape,point0');

        container.sortChildren();

        // Still make sure point1 is below point0 because of shape.
        // This second test prevents a regression, and a bug that occurs only after second call.
        assert.deepStrictEqual(container.children.map(actor => actor.name).join(), 'point1,shape,point0');
    }
}
