import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import Scene from 'engine/Scene/Scene';
import Room from 'engine/Scene/Room/Room';
import RoomConnection from 'engine/Scene/Room/RoomConnection';

@suite
export default class SceneTest {

    @test
    public testGetConnexeRooms() {
        const scene = new Scene();
        let n = 0;

        const rooms = Array(9).fill(0).map(_ => {
            const room = new Room();
            scene.addRoom(`room_${n++}`, room);
            return room;
        });

        scene.addRoomConnection(new RoomConnection([0, 1].map(i => rooms[i]), true));
        scene.addRoomConnection(new RoomConnection([1, 2, 3].map(i => rooms[i]), true));
        scene.addRoomConnection(new RoomConnection([3, 4].map(i => rooms[i]), true));
        scene.addRoomConnection(new RoomConnection([4, 5].map(i => rooms[i]), false));
        scene.addRoomConnection(new RoomConnection([7, 8].map(i => rooms[i]), true));

        const connexe0 = scene.getConnexeRooms(rooms[1]);

        assert.deepEqual(connexe0.length, 5, 'Expected a connexe with 5 rooms');
        assert(connexe0.includes(rooms[0]));
        assert(connexe0.includes(rooms[1]));
        assert(connexe0.includes(rooms[2]));
        assert(connexe0.includes(rooms[3]));
        assert(connexe0.includes(rooms[4]));

        const connexe1 = scene.getConnexeRooms(rooms[8]);

        assert.deepEqual(connexe1.length, 2, 'Expected a connexe with 2 rooms');
        assert(connexe1.includes(rooms[7]));
        assert(connexe1.includes(rooms[8]));
    }

    @test
    public testGetRoomConnection() {
        const scene = new Scene();

        const room0 = new Room();
        const room1 = new Room();
        const room2 = new Room();

        const roomConnection01 = new RoomConnection([room0, room1]);
        const roomConnection02 = new RoomConnection([room0, room2]);
        const roomConnection012 = new RoomConnection([room0, room2, room1]);

        scene.addRoom('room0', room0);
        scene.addRoom('room1', room1);
        scene.addRoom('room2', room2);

        scene.addRoomConnection(roomConnection01);
        scene.addRoomConnection(roomConnection02);
        scene.addRoomConnection(roomConnection012);

        assert.strictEqual(scene.getRoomConnection(['room0', 'room1']), roomConnection01);
        assert.strictEqual(scene.getRoomConnection(['room2', 'room0']), roomConnection02);
        assert.strictEqual(scene.getRoomConnection(['room2', 'room0', 'room1']), roomConnection012);
    }
}
