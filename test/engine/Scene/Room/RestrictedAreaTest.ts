import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import RestrictedArea from 'engine/Scene/Room/RestrictedArea';

@suite
export default class RestrictedAreaTest {

    @test
    public testConstructorSetAreaFromPolygon() {
        const polygonWithoutHole: Polygon = [[
            {x: 0, y: 0},
            {x: 100, y: 0},
            {x: 100, y: 100},
            {x: 0, y: 100},
        ]];

        const polygonWithHole: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 40, y: 40},
                {x: 40, y: 60},
                {x: 60, y: 60},
                {x: 60, y: 40},
            ],
        ];

        assert.deepStrictEqual(
            new RestrictedArea(polygonWithoutHole, 10).getArea(),
            [[
                {x: 10, y: 10},
                {x: 90, y: 10},
                {x: 90, y: 90},
                {x: 10, y: 90},
            ]],
            'RestrictedArea without hole and a radius',
        );

        assert.deepStrictEqual(
            new RestrictedArea(polygonWithHole, 10).getArea(),
            [
                [
                    {x: 10, y: 10},
                    {x: 90, y: 10},
                    {x: 90, y: 90},
                    {x: 10, y: 90},
                ],
                [
                    {x: 30, y: 40},
                    {x: 30, y: 60},
                    {x: 31, y: 62},
                    {x: 31, y: 65},
                    {x: 34, y: 66},
                    {x: 35, y: 69},
                    {x: 38, y: 69},
                    {x: 40, y: 70},
                    {x: 60, y: 70},
                    {x: 62, y: 69},
                    {x: 65, y: 69},
                    {x: 66, y: 66},
                    {x: 69, y: 65},
                    {x: 69, y: 62},
                    {x: 70, y: 60},
                    {x: 70, y: 40},
                    {x: 69, y: 38},
                    {x: 69, y: 35},
                    {x: 66, y: 34},
                    {x: 65, y: 31},
                    {x: 62, y: 31},
                    {x: 60, y: 30},
                    {x: 40, y: 30},
                    {x: 38, y: 31},
                    {x: 35, y: 31},
                    {x: 34, y: 34},
                    {x: 31, y: 35},
                    {x: 31, y: 38},
                ],
            ],
            'RestrictedArea with hole and a radius',
        );

        assert.deepStrictEqual(
            new RestrictedArea(polygonWithoutHole, 0).getArea(),
            [[
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ]],
            'RestrictedArea without hole and no radius',
        );

        assert.deepStrictEqual(
            new RestrictedArea(polygonWithHole, 0).getArea(),
            [
                [
                    {x: 0, y: 0},
                    {x: 100, y: 0},
                    {x: 100, y: 100},
                    {x: 0, y: 100},
                ],
                [
                    {x: 40, y: 40},
                    {x: 40, y: 60},
                    {x: 60, y: 60},
                    {x: 60, y: 40},
                ],
            ],
            'RestrictedArea with hole and no radius',
        );
    }
}
