import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import Points from 'engine/Geometry/Points';

@suite
export default class PointsTest {

    @test
    public testIndexOfPoint() {
        const path: Path = [
            {x: 5, y: 6},
            {x: 7, y: 8},
            {x: 9, y: 4},
        ];

        assert.strictEqual(Points.indexOfPoint(path, {x: 5, y: 6}), 0);
        assert.strictEqual(Points.indexOfPoint(path, {x: 7, y: 8}), 1);
        assert.strictEqual(Points.indexOfPoint(path, {x: 9, y: 4}), 2);
        assert.strictEqual(Points.indexOfPoint(path, {x: 5, y: 42}), -1);
    }

    @test
    public testDistance() {
        assert.equal(Points.distance([{x: 0, y: 0}, {x: 3, y: 4}]), 5);
        assert.equal(Points.distance([{x: 1, y: -1}, {x: 4, y: 3}]), 5);
    }

    @test
    public testNumbersToPoints() {
        const points = Points.numbersToPoints([1, 2, 3, 4]);
        assert.equal(points.length, 2);
        assert.equal(points[0].x, 1);
        assert.equal(points[1].y, 4);
    }
}
