import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import Polygons from 'engine/Geometry/Polygons';

@suite
export default class PolygonsTest {

    @test
    public testSegments() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 100, y: 0},
            {x: 100, y: 100},
            {x: 0, y: 100},
        ]];

        const sides: Segment[] = Polygons.segments(polygon);

        assert.equal(sides.length, 4, 'Expected square polygon have 4 sides');
        assert.deepStrictEqual(sides, [
            [{x: 0, y: 0}, {x: 100, y: 0}],
            [{x: 100, y: 0}, {x: 100, y: 100}],
            [{x: 100, y: 100}, {x: 0, y: 100}],
            [{x: 0, y: 100}, {x: 0, y: 0}],
        ]);
    }

    @test
    public testSegmentsWithAHole() {
        const polygon: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 20, y: 20},
                {x: 40, y: 50},
                {x: 50, y: 10},
            ],
        ];

        const sides: Segment[] = Polygons.segments(polygon);

        assert.equal(sides.length, 7, 'Expected square polygon with triangle hole have 7 sides');
        assert.deepStrictEqual(sides, [
            [{x: 0, y: 0}, {x: 100, y: 0}],
            [{x: 100, y: 0}, {x: 100, y: 100}],
            [{x: 100, y: 100}, {x: 0, y: 100}],
            [{x: 0, y: 100}, {x: 0, y: 0}],

            [{x: 20, y: 20}, {x: 40, y: 50}],
            [{x: 40, y: 50}, {x: 50, y: 10}],
            [{x: 50, y: 10}, {x: 20, y: 20}],
        ]);
    }

    @test
    public testContainsPointForPointsNotOnEdges() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 100, y: 0},
            {x: 100, y: 100},
            {x: 0, y: 100},
        ]];

        assert(Polygons.containsPoint(polygon, {x: 50, y: 50}));
        assert(!Polygons.containsPoint(polygon, {x: -50, y: 50}));
    }

    @test
    public testContainsPointOnPolygonWithHoleForPointsNotOnEdges() {
        const polygon: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 30, y: 30},
                {x: 40, y: 60},
                {x: 70, y: 30},
            ],
        ];

        assert(Polygons.containsPoint(polygon, {x: 10, y: 10}));
        assert(!Polygons.containsPoint(polygon, {x: 32, y: 32}));
    }

    @test
    public testNearestPointInsidePolygonWithSquarePolygon() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 0, y: 100},
            {x: 100, y: 100},
            {x: 100, y: 0},
        ]];

        const nearAngle0 = Polygons.nearestPointInsidePolygon({x: -1, y: -1}, polygon);
        assert.equal(nearAngle0.x, 0);
        assert.equal(nearAngle0.y, 0);

        const farAngle2 = Polygons.nearestPointInsidePolygon({x: 1000, y: 1234}, polygon);
        assert.equal(farAngle2.x, 100);
        assert.equal(farAngle2.y, 100);

        const nearSideMiddle1 = Polygons.nearestPointInsidePolygon({x: 110, y: 50}, polygon);
        assert.equal(nearSideMiddle1.x, 100);
        assert.equal(nearSideMiddle1.y, 50);

        const nearSideMiddle2 = Polygons.nearestPointInsidePolygon({x: 50, y: 1000}, polygon);
        assert.equal(nearSideMiddle2.x, 50);
        assert.equal(nearSideMiddle2.y, 100);
    }

    @test
    public testPaddingWithSquare() {
        const squarePolygon: Polygon = [[
            {x: 0, y: 0},
            {x: 100, y: 0},
            {x: 100, y: 100},
            {x: 0, y: 100},
        ]];

        const smallerSquarePolygon = Polygons.padding(squarePolygon, 15);
        assert.deepStrictEqual(smallerSquarePolygon[0], [
            {x: 15, y: 15},
            {x: 85, y: 15},
            {x: 85, y: 85},
            {x: 15, y: 85},
        ]);
    }

    @test
    public testPaddingWithPrisonCell() {
        const cellPolygon: Polygon = [[
            {x: 263, y: 204},
            {x: 600, y: 24},
            {x: 733, y: 300},
            {x: 412, y: 492},
        ]];

        const paddedPolygon = Polygons.padding(cellPolygon, 20);
        assert.deepStrictEqual(paddedPolygon[0], [
            {x: 290, y: 212},
            {x: 591, y: 51},
            {x: 707, y: 292},
            {x: 420, y: 464},
        ]);
    }

    @test
    public testPaddingWithHole() {
        const polygon: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 30, y: 30},
                {x: 40, y: 60},
                {x: 70, y: 30},
            ],
        ];

        const paddedPolygon = Polygons.padding(polygon, 10);

        assert.deepStrictEqual(paddedPolygon, [
            [
                { x: 10, y: 10 },
                { x: 90, y: 10 },
                { x: 90, y: 90 },
                { x: 10, y: 90 },
            ],
            [
                { x: 21, y: 25 },
                { x: 21, y: 33 },
                { x: 31, y: 63 },
                { x: 34, y: 66 },
                { x: 37, y: 70 },
                { x: 38, y: 70 },
                { x: 42, y: 68 },
                { x: 47, y: 67 },
                { x: 77, y: 37 },
                { x: 77, y: 36 },
                { x: 79, y: 35 },
                { x: 79, y: 31 },
                { x: 80, y: 27 },
                { x: 79, y: 26 },
                { x: 79, y: 25 },
                { x: 75, y: 23 },
                { x: 73, y: 20 },
                { x: 71, y: 21 },
                { x: 70, y: 20 },
                { x: 30, y: 20 },
                { x: 26, y: 22 },
                { x: 23, y: 23 },
            ],
        ]);
    }

    @test
    public testPaddingWithHoleMergeToPolygonSide() {
        const polygon: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 10, y: 10},
                {x: 10, y: 20},
                {x: 20, y: 20},
                {x: 20, y: 10},
            ],
        ];

        const paddedPolygon = Polygons.padding(polygon, 8);

        assert.deepStrictEqual(paddedPolygon, [[
            { x: 8, y: 27 },
            { x: 10, y: 28 },
            { x: 20, y: 28 },
            { x: 22, y: 27 },
            { x: 24, y: 27 },
            { x: 25, y: 25 },
            { x: 27, y: 24 },
            { x: 27, y: 22 },
            { x: 28, y: 20 },
            { x: 28, y: 10 },
            { x: 27, y: 8 },
            { x: 92, y: 8 },
            { x: 92, y: 92 },
            { x: 8, y: 92 },
        ]]);
    }

    @test
    public testPaddingWithHoleMergeToOtherHole() {
        const polygon: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 30, y: 30},
                {x: 30, y: 40},
                {x: 40, y: 40},
                {x: 40, y: 30},
            ],
            [
                {x: 50, y: 30},
                {x: 50, y: 40},
                {x: 60, y: 40},
                {x: 60, y: 30},
            ],
        ];

        const paddedPolygon = Polygons.padding(polygon, 8);

        assert.deepStrictEqual(paddedPolygon, [
            [
                { x: 8, y: 8 },
                { x: 92, y: 8 },
                { x: 92, y: 92 },
                { x: 8, y: 92 },
            ],
            [
                { x: 22, y: 30 },
                { x: 22, y: 40 },
                { x: 23, y: 42 },
                { x: 23, y: 44 },
                { x: 25, y: 45 },
                { x: 26, y: 47 },
                { x: 28, y: 47 },
                { x: 30, y: 48 },
                { x: 40, y: 48 },
                { x: 42, y: 47 },
                { x: 44, y: 47 },
                { x: 45, y: 45 },
                { x: 46, y: 47 },
                { x: 48, y: 47 },
                { x: 50, y: 48 },
                { x: 60, y: 48 },
                { x: 62, y: 47 },
                { x: 64, y: 47 },
                { x: 65, y: 45 },
                { x: 67, y: 44 },
                { x: 67, y: 42 },
                { x: 68, y: 40 },
                { x: 68, y: 30 },
                { x: 67, y: 28 },
                { x: 67, y: 26 },
                { x: 65, y: 25 },
                { x: 64, y: 23 },
                { x: 62, y: 23 },
                { x: 60, y: 22 },
                { x: 50, y: 22 },
                { x: 48, y: 23 },
                { x: 46, y: 23 },
                { x: 45, y: 25 },
                { x: 44, y: 23 },
                { x: 42, y: 23 },
                { x: 40, y: 22 },
                { x: 30, y: 22 },
                { x: 28, y: 23 },
                { x: 26, y: 23 },
                { x: 25, y: 25 },
                { x: 23, y: 26 },
                { x: 23, y: 28 },
            ],
        ]);
    }

    @test
    public testPaddingDoesNotAlterInputPolygon() {
        const polygon: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 30, y: 30},
                {x: 40, y: 60},
                {x: 70, y: 30},
            ],
        ];

        Polygons.padding(polygon, 10);

        assert.deepStrictEqual(polygon, [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 30, y: 30},
                {x: 40, y: 60},
                {x: 70, y: 30},
            ],
        ]);
    }

    @test
    public testGetConcavePoints() {
        const polygon: Polygon = [[
            {x: 100, y: 300},
            {x: 0, y: 300},
            {x: 0, y: 0},
            {x: 400, y: 0},
            {x: 400, y: 300},
            {x: 300, y: 300},
            {x: 300, y: 100},
            {x: 100, y: 100},
        ]];

        const concavePoints = Polygons.getConcavePoints(polygon);

        assert.equal(concavePoints.length, 2, 'I should have 2 concave points');
        assert.deepStrictEqual(concavePoints[0], {x: 300, y: 100}, 'First point should be 300, 100');
        assert.deepStrictEqual(concavePoints[1], {x: 100, y: 100}, 'Second point should be 100, 100');
    }

    @test
    public testGetConcavePointsWithHole() {
        const polygon: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 90},
                {x: 90, y: 90},
                {x: 90, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 30, y: 30},
                {x: 40, y: 60},
                {x: 70, y: 30},
            ],
        ];

        const concavePoints = Polygons.getConcavePoints(polygon);

        assert.equal(concavePoints.length, 4, 'I should have 4 concave points (1 from first ring, 3 from hole)');
        assert.deepStrictEqual(concavePoints, [
            {x: 90, y: 90},
            {x: 40, y: 60},
            {x: 70, y: 30},
            {x: 30, y: 30},
        ]);
    }

    @test
    public testSegmentInsidePolygonWithSquare() {
        const square: Polygon = [[
            {x: 0, y: 0},
            {x: 100, y: 0},
            {x: 100, y: 100},
            {x: 0, y: 100},
        ]];

        assert(Polygons.segmentInsidePolygon(square, [{x: 25, y: 25}, {x: 75, y: 75}]));
        assert(Polygons.segmentInsidePolygon(square, [{x: 75, y: 75}, {x: 25, y: 25}]));
        assert(Polygons.segmentInsidePolygon(square, [{x: 0, y: 0}, {x: 0, y: 100}]));
        assert(Polygons.segmentInsidePolygon(square, [{x: 0, y: 100}, {x: 0, y: 0}]));
        assert(Polygons.segmentInsidePolygon(square, [{x: 0, y: 0}, {x: 100, y: 100}]));
        assert(Polygons.segmentInsidePolygon(square, [{x: 100, y: 100}, {x: 0, y: 0}]));
        assert(!Polygons.segmentInsidePolygon(square, [{x: -10, y: -10}, {x: -20, y: -10}]));
    }

    @test
    public testSegmentInsidePolygonWithConcavePolygon() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 400, y: 0},
            {x: 400, y: 300},
            {x: 300, y: 300},
            {x: 300, y: 100},
            {x: 100, y: 100},
            {x: 100, y: 300},
            {x: 0, y: 300},
        ]];

        assert(Polygons.segmentInsidePolygon(polygon, [{x: 300, y: 100}, {x: 300, y: 300}]));
        assert(Polygons.segmentInsidePolygon(polygon, [{x: 300, y: 300}, {x: 300, y: 100}]));
        assert(Polygons.segmentInsidePolygon(polygon, [{x: 300, y: 300}, {x: 400, y: 300}]));
        assert(Polygons.segmentInsidePolygon(polygon, [{x: 400, y: 300}, {x: 300, y: 300}]));

        assert(!Polygons.segmentInsidePolygon(polygon, [{x: 300, y: 300}, {x: 100, y: 100}]));
        assert(!Polygons.segmentInsidePolygon(polygon, [{x: 100, y: 100}, {x: 300, y: 300}]));
    }

    @test
    public testSegmentInsidePolygonWithConcaveCornerPolygon() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 400, y: 0},
            {x: 400, y: 300},
            {x: 300, y: 300},
            {x: 200, y: 100},
            {x: 100, y: 300},
            {x: 0, y: 300},
        ]];

        assert(Polygons.segmentInsidePolygon(polygon, [{x: 200, y: 100}, {x: 300, y: 200}]));
        assert(Polygons.segmentInsidePolygon(polygon, [{x: 300, y: 200}, {x: 200, y: 100}]));

        assert(Polygons.segmentInsidePolygon(polygon, [{x: 100, y: 200}, {x: 200, y: 100}]));
        assert(Polygons.segmentInsidePolygon(polygon, [{x: 200, y: 100}, {x: 100, y: 200}]));
    }

    @test
    public testSegmentInsidePolygonWithHole() {
        const polygon: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 30, y: 30},
                {x: 40, y: 60},
                {x: 70, y: 30},
            ],
        ];

        assert(Polygons.segmentInsidePolygon(polygon, [{x: 1, y: 1}, {x: 10, y: 10}]));
        assert(Polygons.segmentInsidePolygon(polygon, [{x: 0, y: 0}, {x: 10, y: 10}]));
        assert(!Polygons.segmentInsidePolygon(polygon, [{x: 1, y: 1}, {x: -10, y: 10}]));
        assert(!Polygons.segmentInsidePolygon(polygon, [{x: 1, y: 1}, {x: 99, y: 99}]));
        assert(Polygons.segmentInsidePolygon(polygon, [{x: 30, y: 30}, {x: 70, y: 30}]));
        assert(!Polygons.segmentInsidePolygon(polygon, [{x: 30, y: 30}, {x: 50, y: 35}]));
        assert(Polygons.segmentInsidePolygon(polygon, [{x: 30, y: 30}, {x: 50, y: 25}]));
        assert(Polygons.segmentInsidePolygon(polygon, [{x: 0, y: 0}, {x: 30, y: 30}]));
        assert(!Polygons.segmentInsidePolygon(polygon, [{x: 0, y: 0}, {x: 32, y: 32}]));
    }

    @test
    public testSegmentInsidePolygonWithConcaveHole() {
        const polygon: Polygon = [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 30, y: 20},
                {x: 30, y: 30},
                {x: 20, y: 30},
                {x: 20, y: 60},
                {x: 50, y: 60},
                {x: 50, y: 50},
                {x: 60, y: 50},
                {x: 60, y: 20},
            ],
        ];

        assert(Polygons.segmentInsidePolygon(polygon, [{x: 1, y: 1}, {x: 10, y: 10}]));
        assert(Polygons.segmentInsidePolygon(polygon, [{x: 50, y: 60}, {x: 60, y: 50}]));
        assert(!Polygons.segmentInsidePolygon(polygon, [{x: 50, y: 50}, {x: 30, y: 30}]));
        assert(!Polygons.segmentInsidePolygon(polygon, [{x: 30, y: 30}, {x: 50, y: 50}]));
    }

    @test
    public testSegmentInsidePolygonWithPaddedPolygonWithHole() {
        const polygon: Polygon = [
            [
                { x: 12, y: 12 },
                { x: 88, y: 12 },
                { x: 88, y: 88 },
                { x: 12, y: 88 },
            ],
            [
                { x: 28, y: 40 },
                { x: 28, y: 60 },
                { x: 30, y: 63 },
                { x: 30, y: 66 },
                { x: 32, y: 68 },
                { x: 34, y: 70 },
                { x: 37, y: 70 },
                { x: 40, y: 72 },
                { x: 60, y: 72 },
                { x: 63, y: 70 },
                { x: 66, y: 70 },
                { x: 68, y: 68 },
                { x: 70, y: 66 },
                { x: 70, y: 63 },
                { x: 72, y: 60 },
                { x: 72, y: 40 },
                { x: 70, y: 37 },
                { x: 70, y: 34 },
                { x: 68, y: 32 },
                { x: 66, y: 30 },
                { x: 63, y: 30 },
                { x: 60, y: 28 },
                { x: 40, y: 28 },
                { x: 37, y: 30 },
                { x: 34, y: 30 },
                { x: 32, y: 32 },
                { x: 30, y: 34 },
                { x: 30, y: 37 },
            ],
        ];

        assert(Polygons.segmentInsidePolygon(polygon, [{ x: 45, y: 15 }, { x: 63, y: 30 }]));
        assert(!Polygons.segmentInsidePolygon(polygon, [{ x: 63, y: 30 }, { x: 37, y: 70 }]));
    }

    @test
    public testDifference() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 100, y: 0},
            {x: 100, y: 100},
            {x: 0, y: 100},
        ]];

        const obstacle: Polygon = [[
            {x: 40, y: 40},
            {x: 60, y: 40},
            {x: 60, y: 60},
            {x: 40, y: 60},
        ]];

        const difference = Polygons.difference(polygon, obstacle);

        assert.deepStrictEqual(difference, [
            [
                {x: 0, y: 0},
                {x: 100, y: 0},
                {x: 100, y: 100},
                {x: 0, y: 100},
            ],
            [
                {x: 40, y: 40},
                {x: 40, y: 60},
                {x: 60, y: 60},
                {x: 60, y: 40},
            ],
        ]);
    }

    @test
    public testDifference2() {
        const polygon: Polygon = [[
            {x: 0, y: 0},
            {x: 400, y: 0},
            {x: 400, y: 400},
            {x: 0, y: 400},
        ]];

        const obstacle: Polygon = [[
            {x: 150, y: 150},
            {x: 210, y: 150},
            {x: 210, y: 210},
            {x: 150, y: 210},
        ]];

        const difference = Polygons.difference(polygon, obstacle);

        assert.deepStrictEqual(difference, [
            [
                {x: 0, y: 0},
                {x: 400, y: 0},
                {x: 400, y: 400},
                {x: 0, y: 400},
            ],
            [
                {x: 150, y: 150},
                {x: 150, y: 210},
                {x: 210, y: 210},
                {x: 210, y: 150},
            ],
        ]);
    }

    @test
    public testUnionWithTwoPolygonAndASingleSegmentDoor() {
        const a: Polygon = [[
            {x: 0, y: 0},
            {x: 100, y: 0},

            // Door
            {x: 200, y: 100},
            {x: 200, y: 200},

            {x: 100, y: 300},
            {x: 0, y: 300},
        ]];

        const b: Polygon = [[
            {x: 400, y: 0},
            {x: 400, y: 300},
            {x: 300, y: 300},

            // Door
            {x: 200, y: 200},
            {x: 200, y: 100},

            {x: 300, y: 0},
        ]];

        const union: Polygon = Polygons.union(a, b);

        assert.deepStrictEqual(union, [[
            {x: 0, y: 0},
            {x: 100, y: 0},
            {x: 200, y: 100},
            {x: 300, y: 0},
            {x: 400, y: 0},
            {x: 400, y: 300},
            {x: 300, y: 300},
            {x: 200, y: 200},
            {x: 100, y: 300},
            {x: 0, y: 300},
        ]]);
    }
}
