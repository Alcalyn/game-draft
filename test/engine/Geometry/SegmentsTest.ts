import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import Segments from 'engine/Geometry/Segments';

@suite
export default class SegmentsTest {

    @test
    public testNearestPoint() {
        const segment: Segment = [
            {x: 0, y: 0},
            {x: 10, y: 0},
        ];

        const middlePoint = Segments.nearestPoint(segment, {x: 5, y: 2});
        assert.equal(middlePoint.x, 5);
        assert.equal(middlePoint.y, 0);

        const leftPoint = Segments.nearestPoint(segment, {x: -12, y: -15});
        assert.equal(leftPoint.x, 0);
        assert.equal(leftPoint.y, 0);

        const farPoint = Segments.nearestPoint(segment, {x: 7, y: 142});
        assert.equal(farPoint.x, 7);
        assert.equal(farPoint.y, 0);
    }

    @test
    public testNearestPointWithDiagonalSegment() {
        const segment: Segment = [
            {x: 10, y: 0},
            {x: 20, y: 10},
        ];

        const middlePoint = Segments.nearestPoint(segment, {x: 10, y: 10});
        assert.equal(middlePoint.x, 15);
        assert.equal(middlePoint.y, 5);

        const leftPoint = Segments.nearestPoint(segment, {x: -12, y: -15});
        assert.equal(leftPoint.x, 10);
        assert.equal(leftPoint.y, 0);
    }

    @test
    public testCrossLine() {
        const p = (x: number, y: number) => ({x, y});

        assert(Segments.crossLine([p(20, 5), p(20, -5)], [p(0, 0), p(10, 0)]));
        assert(!Segments.crossLine([p(20, 5), p(20, 15)], [p(0, 0), p(10, 0)]));
        assert(!Segments.crossLine([p(20, -5), p(20, -15)], [p(0, 0), p(10, 0)]));
    }

    @test
    public testCross() {
        const left = {x: 0, y: 50};
        const right = {x: 100, y: 50};
        const top = {x: 50, y: 0};
        const bottom = {x: 50, y: 100};

        assert(Segments.cross([left, right], [top, bottom]));
        assert(!Segments.cross([left, bottom], [top, right]));

        assert(Segments.cross(
            [{x: 50, y: 50}, {x: 400, y: 300}],
            [{x: 300, y: 100}, {x: 100, y: 300}],
        ));
    }

    @test
    public testSegmentsCrossShouldReturnsFalseWhenCrossingAtPoint() {
        const left = {x: 0, y: 50};
        const right = {x: 100, y: 50};
        const top = {x: 50, y: 0};

        assert(!Segments.cross([left, right], [top, right]));
        assert(!Segments.cross(
            [{x: 100, y: 200}, {x: 0, y: 0}],
            [{x: 400, y: 0}, {x: 0, y: 0}],
        ));
        assert(!Segments.cross(
            [{x: 100, y: 200}, {x: 0, y: 0}],
            [{x: 0, y: 0}, {x: 400, y: 0}],
        ));
        assert(!Segments.cross(
            [{x: 300, y: 100}, {x: 100, y: 300}],
            [{x: 100, y: 300}, {x: 400, y: 300}],
        ));
    }

    @test
    public testLinesBoundingBoxesIntersect() {
        assert(!Segments.boundingBoxesIntersect(
            [{x: 2, y: 2}, {x: 4, y: 6}],
            [{x: 7, y: 2}, {x: 12, y: 6}],
        ));

        assert(Segments.boundingBoxesIntersect(
            [{x: 2, y: 2}, {x: 4, y: 6}],
            [{x: 3, y: 3}, {x: 6, y: 4}],
        ));

        assert(Segments.boundingBoxesIntersect(
            [{x: 50, y: 50}, {x: 400, y: 300}],
            [{x: 0, y: 300}, {x: 300, y: 100}],
        ));

        // False when touching point
        assert(!Segments.boundingBoxesIntersect(
            [{x: 2, y: 2}, {x: 4, y: 4}],
            [{x: 4, y: 4}, {x: 6, y: 6}],
        ));
    }
}
