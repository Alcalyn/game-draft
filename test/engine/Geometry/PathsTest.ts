import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import Paths from 'engine/Geometry/Paths';

@suite
export default class PathsTest {

    @test
    public testisPointAbove() {
        const shape: Path = [
            {x: 0, y: 0},
            {x: 10, y: 10},
            {x: 20, y: 10},
            {x: 30, y: 0},
        ];

        assert.deepEqual(Paths.isPointAbove(shape, {x: 5, y: 6}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 5, y: 4}), false);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 15, y: 11}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 15, y: 9}), false);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 25, y: 6}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 25, y: 4}), false);

        // With point at left or right of the path
        assert.deepEqual(Paths.isPointAbove(shape, {x: -5, y: -4}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: -5, y: -6}), false);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 40, y: -9}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 40, y: -11}), false);
    }

    @test
    public testisPointAboveWithPathRightToLeft() {
        const shape: Path = [
            {x: 30, y: 0},
            {x: 20, y: 10},
            {x: 10, y: 10},
            {x: 0, y: 0},
        ];

        assert.deepEqual(Paths.isPointAbove(shape, {x: 5, y: 6}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 5, y: 4}), false);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 15, y: 11}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 15, y: 9}), false);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 25, y: 6}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 25, y: 4}), false);

        // With point at left or right of the path
        assert.deepEqual(Paths.isPointAbove(shape, {x: -5, y: -4}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: -5, y: -6}), false);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 40, y: -9}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 40, y: -11}), false);
    }

    @test
    public testisPointAboveWithPointAtSideLimit() {
        const shape: Path = [
            {x: 0, y: 0},
            {x: 10, y: 10},
            {x: 20, y: 10},
            {x: 30, y: 0},
        ];

        assert.deepEqual(Paths.isPointAbove(shape, {x: 0, y: 6}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 0, y: -6}), false);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 30, y: 6}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 30, y: -6}), false);
    }

    @test
    public testisPointAboveWithPathHavingTwoPoints() {
        const shape: Path = [
            {x: 0, y: 0},
            {x: 10, y: 10},
        ];

        assert.deepEqual(Paths.isPointAbove(shape, {x: -5, y: 0}), true);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 5, y: 0}), false);
        assert.deepEqual(Paths.isPointAbove(shape, {x: 15, y: 0}), false);
    }
}
