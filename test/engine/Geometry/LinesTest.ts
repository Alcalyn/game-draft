import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import Lines from 'engine/Geometry/Lines';

@suite
export default class LinesTest {

    @test
    public testPointDistance() {
        assert.equal(Lines.pointDistance({x: 5, y: 1}, [{x: 0, y: 2}, {x: 10, y: 2}]), 1);
        assert.equal(Lines.pointDistance({x: 0, y: 0}, [{x: 0, y: 1}, {x: 1, y: 0}]), Math.SQRT1_2);
    }

    @test
    public testIsPointOnRight() {
        const p = (x: number, y: number) => ({x, y});

        assert(Lines.isPointOnRight([p(0, 0), p(10, 0)], p(5, 5)));
        assert(!Lines.isPointOnRight([p(0, 0), p(10, 0)], p(5, -5)));

        assert(!Lines.isPointOnRight([p(10, 0), p(0, 0)], p(5, 5)));
        assert(Lines.isPointOnRight([p(10, 0), p(0, 0)], p(5, -5)));

        assert(!Lines.isPointOnRight([p(0, 0), p(10, 0)], p(5, 0)));
        assert(Lines.isPointOnRight([p(0, 0), p(10, 0)], p(5, 0), true));
    }
}
