import { suite, test } from 'mocha-typescript';
import assert from 'assert';
import Utils from 'engine/Utils';

@suite
export default class UtilsTest {

    @test
    public testChunkPair() {
        const chunks = Utils.chunkPair([1, 2, 3, 4, 5, 6]);

        assert.deepStrictEqual(chunks, [
            [1, 2],
            [3, 4],
            [5, 6],
        ]);
    }
}
