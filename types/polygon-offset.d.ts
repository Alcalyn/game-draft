declare class Offset {
    static degreesToUnits(degrees: any, units: any): any;
    static orientRings(coordinates: any, depth: any, isHole: any): any;
    constructor(vertices?: any, arcSegments?: any);
    vertices: any;
    edges: any;
    arcSegments(arcSegments: any): any;
    createArc(vertices: any, center: any, radius: any, startVertex: any, endVertex: any, segments: any, outwards: any): any;
    data(vertices: number[][]): any;
    distance(dist: number, units: any): any;
    ensureLastPoint(vertices: any): any;
    margin(dist: number): number[][];
    offset(dist: number): number[][];
    offsetContour(curve: any, edges: any): any;
    offsetLine(dist: number): any;
    offsetLines(dist: number): any;
    offsetPoint(distance: any): any;
    padding(dist: number): number[][];
    validate(vertices: any): any;
}

export default Offset;
