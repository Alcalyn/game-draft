## Design files

Only original files that are needed to create assets.


### Animating

- Using Blender with COA Tools plugin.
- Export as DragonBones, Bake animation every 1 images.
- Animation are read with Pixi and DragonBonesJS.

### To convert Krita to pngs, using ImageMagick (`convert`):

- From Krita, export as TIF, uncheck "Flatten image"
- To extract layers from tif: `convert -trim file.tif file.png`
