/* tslint:disable:max-classes-per-file rule2 rule3... */
import { Application, Point as PixiPoint, Texture, Sprite, Graphics, InteractionEvent } from 'pixi.js';
import { Viewport } from 'pixi-viewport';
import Rings from 'engine/Geometry/Rings';
import Paths from 'engine/Geometry/Paths';

export class App extends Application {
    public viewport: Viewport;

    public objectClassName: string;
    public textureUrl: string;
    public texture: Texture;

    public sprite: Sprite;

    public modules: {[key: string]: Module} = {};
    public currentModule: Module = null;

    constructor() {
        super({
            width: window.innerWidth,
            height: window.innerHeight - 40,
            backgroundColor: 0xffffff,
            autoStart: true,
            sharedTicker: true,
            sharedLoader: true,
            antialias: true,
        });

        document.body.appendChild(this.view);

        this.viewport = new Viewport({
            screenWidth: window.innerWidth,
            screenHeight: window.innerHeight,
            interaction: this.renderer.plugins.interaction,
        });

        this.stage.addChild(this.viewport);
        this.displayOrigin();

        this.textureUrl = prompt('Texture url (i.e "assets/scenes/prison/door.png")', 'assets/scenes/prison/bed.png');
        this.objectClassName = prompt('Object class name (i.e "Door", "Bed")', 'Bed');
        this.texture = Texture.from(this.textureUrl);
        this.sprite = Sprite.from(this.texture);

        const center: PixiPoint = new PixiPoint();

        this.viewport.drag();
        this.viewport.wheel({
            center,
        });
        this.viewport.moveCenter(center);

        this.viewport.addChild(this.sprite);

        this.stage.interactive = true;

        this.modules.anchor = new AnchorModule(this);
        this.modules.obstacle = new ObstacleModule(this);
        this.modules.shape = new ShapeModule(this);

        this.createModulesButtons();
        this.addDumpButton();
    }

    private createModulesButtons(): void {
        const buttonsContainer = document.querySelector('body');

        Object.values(this.modules).forEach((module: Module) => {
            const button = document.createElement('button');

            button.textContent = module.constructor.name;

            button.addEventListener('click', () => {
                if (module === this.currentModule) {
                    this.currentModule.stop();
                    this.currentModule = null;
                    return;
                }

                if (null !== this.currentModule) {
                    this.currentModule.stop();
                    this.currentModule = null;
                }

                module.start();

                this.currentModule = module;
            });

            buttonsContainer.appendChild(button);
        });
    }

    private displayOrigin(): void {
        const g: Graphics = new Graphics();

        g.lineStyle(2, 0x000000, 0.5);

        g.drawCircle(0, 0, 20);
        g.moveTo(-40, 0);
        g.lineTo(40, 0);
        g.moveTo(0, -40);
        g.lineTo(0, 40);

        this.viewport.addChild(g);
    }

    private addDumpButton(): void {
        const buttonsContainer = document.querySelector('body');
        const button = document.createElement('button');

        button.textContent = 'Dump';

        button.addEventListener('click', () => {
            const moduleObstacle: ObstacleModule = (this.modules.obstacle as ObstacleModule);
            const moduleShape: ShapeModule = (this.modules.shape as ShapeModule);
            const isObstacle: boolean = moduleObstacle.obstacleRing.length > 0;
            const parentClass: string = isObstacle ? 'Obstacle' : 'Actor';
            const polygonAttribute: string[] = [];
            const shapeAttribute: string[] = [];
            let setShape: string = '';
            let setObstacle: string = '';

            if (isObstacle) {
                polygonAttribute.push('    private static obstaclePolygon: Polygon = [[');
                moduleObstacle.obstacleRing.forEach(point => {
                    polygonAttribute.push(`        {x: ${point.x}, y: ${point.y}},`);
                });
                polygonAttribute.push('    ]];\n');

                setObstacle = `        this.setObstaclePolygon(${this.objectClassName}.obstaclePolygon);\n`;
            }

            if (moduleShape.shape.length > 0) {
                shapeAttribute.push('    private static shape: Path = [');
                moduleShape.shape.forEach(point => {
                    shapeAttribute.push(`        {x: ${point.x}, y: ${point.y}},`);
                });
                shapeAttribute.push('    ];\n');

                setShape = `        this.setShape(${this.objectClassName}.shape);\n`;
            }

            const fileContent = App.template()
                .replace(/__OBSTACLE_POLYGON_ATTRIBUTE__\n\n/g, polygonAttribute.join('\n'))
                .replace(/__OBSTACLE_SHAPE_ATTRIBUTE__\n\n/g, shapeAttribute.join('\n'))
                .replace(/__SET_SHAPE__\n/g, setShape)
                .replace(/__SET_OBSTACLE__\n/g, setObstacle)
                .replace(/__PARENT_CLASS__/g, parentClass)
                .replace(/__OBJECT_NAME__/g, this.objectClassName)
                .replace(/__TEXTURE_URL__/g, this.textureUrl)
                .replace(/__SPRITE_ANCHOR_X__/g, this.sprite.anchor.x + '')
                .replace(/__SPRITE_ANCHOR_Y__/g, this.sprite.anchor.y + '')
            ;

            console.log(fileContent);
        });

        buttonsContainer.appendChild(button);
    }

    private static template() {
        return `
import { Point as PixiPoint, Texture } from 'pixi.js';
import __PARENT_CLASS__ from 'engine/Scene/Object/__PARENT_CLASS__';

export default class __OBJECT_NAME__ extends __PARENT_CLASS__ {
__OBSTACLE_POLYGON_ATTRIBUTE__

__OBSTACLE_SHAPE_ATTRIBUTE__

    private static textureUrl: string = '__TEXTURE_URL__';
    private static textureAnchor: PixiPoint = new PixiPoint(
        __SPRITE_ANCHOR_X__,
        __SPRITE_ANCHOR_Y__,
    );

    public constructor() {
        const texture: Texture = Texture.from(__OBJECT_NAME__.textureUrl);
        texture.defaultAnchor = __OBJECT_NAME__.textureAnchor;

        super(texture);

__SET_SHAPE__
__SET_OBSTACLE__
    }
}
        `;
    }
}

abstract class Module {
    protected app: App;
    protected stopCallback: () => void = null;

    public constructor(app: App) {
        this.app = app;

        console.log('init ' + this.constructor.name);
        this.doInit();
    }

    protected abstract doInit(): void;

    protected abstract doStart(): () => void;

    public start(): void {
        console.log('start ' + this.constructor.name);

        if (null !== this.stopCallback) {
            throw new Error('Module already started.');
        }

        this.stopCallback = this.doStart();
    }

    public stop(): void {
        console.log('stop ' + this.constructor.name);
        this.stopCallback();
        this.stopCallback = null;
    }
}

class AnchorModule extends Module {
    protected doInit(): void {
        // noop
    }

    protected doStart(): () => void {
        const callback = (event: InteractionEvent): void => {
            const point: PixiPoint = this.app.viewport.toWorld(event.data.global);

            this.app.sprite.anchor.x += point.x / this.app.texture.width;
            this.app.sprite.anchor.y += point.y / this.app.texture.height;
        };

        this.app.stage.on('click', callback);

        this.app.viewport.plugins.pause('drag');

        return (): void => {
            this.app.stage.off('click', callback);
            this.app.viewport.plugins.resume('drag');
        };
    }
}

class ObstacleModule extends Module {
    public obstacleRing: Ring;
    private obstacleDisplay: Sprite;
    private g: Graphics;

    protected doInit(): void {
        this.obstacleRing = [];
        this.obstacleDisplay = new Sprite();
        this.g = new Graphics();
        this.initGraphics();
        this.obstacleDisplay.addChild(this.g);

        this.app.viewport.addChild(this.obstacleDisplay);
    }

    protected doStart(): () => void {
        const addPointCallback = (event: InteractionEvent): void => {
            const point: PixiPoint = this.app.viewport.toWorld(event.data.global);

            this.obstacleRing.push({
                x: Math.round(point.x),
                y: Math.round(point.y),
            });

            this.updateObstacleDisplay();
        };

        const removePointCallback = (): void => {
            this.obstacleRing.pop();

            this.updateObstacleDisplay();
        };

        this.app.stage.on('click', addPointCallback);
        this.app.stage.on('rightclick', removePointCallback);

        return (): void => {
            this.app.stage.off('click', addPointCallback);
            this.app.stage.off('rightclick', removePointCallback);
        };
    }

    private updateObstacleDisplay(): void {
        this.initGraphics();

        this.obstacleRing.forEach((point: Point) => {
            this.g.drawCircle(point.x, point.y, 4);
        });

        Rings.segments(this.obstacleRing).forEach((segment: Segment) => {
            this.g.moveTo(segment[0].x, segment[0].y);
            this.g.lineTo(segment[1].x, segment[1].y);
        });
    }

    private initGraphics(): void {
        this.g.clear();
        this.g.lineStyle(2, 0xFF0000);
    }
}

class ShapeModule extends Module {
    public shape: Path;
    private shapeDisplay: Sprite;
    private g: Graphics;

    protected doInit(): void {
        this.shape = [];
        this.shapeDisplay = new Sprite();
        this.g = new Graphics();
        this.initGraphics();
        this.shapeDisplay.addChild(this.g);

        this.app.viewport.addChild(this.shapeDisplay);
    }

    protected doStart(): () => void {
        const addPointCallback = (event: InteractionEvent): void => {
            const point: PixiPoint = this.app.viewport.toWorld(event.data.global);

            this.shape.push({
                x: Math.round(point.x),
                y: Math.round(point.y),
            });

            this.updateShapeDisplay();
        };

        const removePointCallback = (): void => {
            this.shape.pop();

            this.updateShapeDisplay();
        };

        this.app.stage.on('click', addPointCallback);
        this.app.stage.on('rightclick', removePointCallback);

        return (): void => {
            this.app.stage.off('click', addPointCallback);
            this.app.stage.off('rightclick', removePointCallback);
        };
    }

    private updateShapeDisplay(): void {
        this.initGraphics();

        this.shape.forEach((point: Point) => {
            this.g.drawCircle(point.x, point.y, 4);
        });

        Paths.segments(this.shape).forEach((segment: Segment) => {
            this.g.moveTo(segment[0].x, segment[0].y);
            this.g.lineTo(segment[1].x, segment[1].y);
        });
    }

    private initGraphics(): void {
        this.g.clear();
        this.g.lineStyle(2, 0x00FF00);
    }
}
