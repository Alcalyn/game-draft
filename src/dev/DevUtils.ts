import { Graph } from 'dijkstra-pathfinder';
import { Graphics, Container, Text, Point as PixiPoint } from 'pixi.js';
import { Viewport } from 'pixi-viewport';
import { inspect } from 'util';

export default class DevUtils {

    /**
     * Quickly create polygons by clicking on viewport,
     * then right click to generate copy-pastable code like:
     * {x: ...},
     * {x: ...},
     * ...
     */
    public static enablePolygonCreator(viewport: Viewport): void {
        let points: Path = [];

        viewport.drag();
        viewport.plugins.remove('follow');

        viewport.on('click', event => {
            points.push(viewport.toWorld(event.data.global));
        });
        viewport.on('rightclick', () => {
            console.log(points.map(point => `{x: ${point.x}, y: ${point.y}},`).join('\n'));
            points = [];
        });
        console.log('Polygon creator enabled. Right click to dump polygon and reset.');
    }

    public static enableCoordsOnClick(viewport: Viewport, display: (point: Point) => string = null): void {
        console.log('Enabled coords displaying on click.');

        if (null === display) {
            display = p => `Clicked at(${p.x}, ${p.y})`;
        }

        viewport.on('click', event => {
            const point = viewport.toWorld(event.data.global);

            console.log(display(point));
        });
    }

    /**
     * console.log with depth relations.
     */
    public static dump(data: any, depth: number = 6): void {
        console.log(inspect(data, false, depth, true));
    }

    private static gSingleton: Graphics = null;

    public static get g(): Graphics {
        DevUtils.initGraphics();

        return DevUtils.gSingleton;
    }

    private static initGraphics(): void {
        if (null !== DevUtils.gSingleton) {
            return;
        }

        DevUtils.gSingleton = new Graphics();

        DevUtils.drawReset();
    }

    public static enableDraw(drawOn: Container): Graphics {
        DevUtils.initGraphics();

        drawOn.addChild(DevUtils.gSingleton);

        return DevUtils.gSingleton;
    }

    public static drawReset(): void {
        DevUtils.gSingleton.clear();
        DevUtils.gSingleton.removeChildren();

        DevUtils.gSingleton.visible = true;
        DevUtils.gSingleton.lineStyle(1, 0x666666);
    }

    public static drawSegment(segment: Segment, color: number = null, width: number = null): void {
        const lastColor = DevUtils.g.line.color;
        const lastWidth = DevUtils.g.line.width;

        if (color !== null) {
            DevUtils.g.line.color = color;
        }

        if (width !== null) {
            DevUtils.g.line.width = width;
        }

        DevUtils.g.moveTo(segment[0].x, segment[0].y);
        DevUtils.g.lineTo(segment[1].x, segment[1].y);

        DevUtils.g.line.color = lastColor;
        DevUtils.g.line.width = lastWidth;
    }

    public static drawPoint(point: Point): void {
        DevUtils.g.drawCircle(point.x, point.y, 6);

        const text = new Text(
            [point.x, point.y].join('\n'),
            {
                fontSize: 12,
                fill: DevUtils.g.line.color,
            },
        );

        text.position = new PixiPoint(point.x, point.y);
        text.position.x += 5;
        text.position.y += 5;

        DevUtils.g.addChild(text);
    }

    public static drawPath(path: Path, color: number = null, width: number = null): void {
        const lastColor = DevUtils.g.line.color;
        const lastWidth = DevUtils.g.line.width;

        if (color !== null) {
            DevUtils.g.line.color = color;
        }

        if (width !== null) {
            DevUtils.g.line.width = width;
        }

        DevUtils.g.moveTo(path[0].x, path[0].y);

        path.forEach(point => {
            DevUtils.g.lineTo(point.x, point.y);
        });

        DevUtils.g.lineTo(path[0].x, path[0].y);

        DevUtils.g.line.color = lastColor;
        DevUtils.g.line.width = lastWidth;
    }

    public static drawPolygon(polygon: Polygon, color: number = null, width: number = null): void {
        polygon.forEach(path => {
            DevUtils.drawPath(path, color, width);
        });
    }

    public static drawGraph(graph: Graph, color: number = null, width: number = null): void {
        graph.nodes.forEach(node => {
            DevUtils.drawPoint(node.payload);
            node.arcs.forEach(arc => {
                DevUtils.drawSegment([node.payload, arc.nodeTo.payload], color, width);
            });
        });
    }
}
