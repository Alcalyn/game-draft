import 'engine/Geometry/Types';
import Utils from 'engine/Utils';

export default class Points {

    public static equals(a: Point, b: Point): boolean {
        return a.x === b.x && a.y === b.y;
    }

    public static indexOfPoint(path: Path, point: Point): number {
        let i = 0;

        while (!Points.equals(point, path[i]) && ++i < path.length);

        return i === path.length ? -1 : i;
    }

    public static operation(a: Point, b: Point, operation: (x: number, y: number) => number): Point {
        return {
            x: operation(a.x, b.x),
            y: operation(a.y, b.y),
        };
    }

    public static difference(a: Point, b: Point): Point {
        return Points.operation(a, b, (x, y) => (x - y));
    }

    public static sum(a: Point, b: Point): Point {
        return {
            x: a.x + b.x,
            y: a.y + b.y,
        };
    }

    public static distance(segment: Segment): number {
        return Math.hypot(segment[1].x - segment[0].x, segment[1].y - segment[0].y);
    }

    public static toArray(point: Point): [number, number] {
        return [point.x, point.y];
    }

    public static numbersToPoints(numbers: number[]): Point[] {
        return Utils
            .chunkPair(numbers)
            .map(pair => ({x: pair[0], y: pair[1]}))
        ;
    }

    public static dotProduct(a: Point, b: Point): number {
        return a.x * b.x + a.y * b.y;
    }

    public static crossProduct(a: Point, b: Point): number {
        return a.x * b.y - b.x * a.y;
    }
}
