import pointInPolygon from 'point-in-polygon';
import {
    MultiPolygon,
    Polygon as ArrayPointsPolygon,
    difference as polygonDifference,
    union as polygonUnion,
} from 'polygon-clipping';
import Points from 'engine/Geometry/Points';
import Segments from 'engine/Geometry/Segments';
import Rings from 'engine/Geometry/Rings';
import Lines from 'engine/Geometry/Lines';

export default class Polygons {

    public static transformPoints<T>(polygon: Polygon, transform: (point: Point) => T): T[][] {
        return polygon.map(path => path.map(transform));
    }

    public static clone(polygon: Polygon): Polygon {
        return Polygons.transformPoints(polygon, point => ({...point}));
    }

    public static pointsToArray(polygon: Polygon): [number, number][][] {
        return Polygons.transformPoints<[number, number]>(polygon, (point: Point): [number, number] => [
            point.x,
            point.y,
        ]);
    }

    public static arrayPointsToPoints(arrayPolygon: [number, number][][]): Polygon {
        return arrayPolygon.map(
            path => path.map(
                point => ({
                    x: point[0],
                    y: point[1],
                }),
            ),
        );
    }

    public static scale(polygon: Polygon, scale: number): Polygon {
        return Polygons.transformPoints(polygon, point => ({
            x: point.x * scale,
            y: point.y * scale,
        }));
    }

    /**
     * Whether point is one of polygon points.
     */
    public static hasPoint(polygon: Polygon, point: Point): boolean {
        let i: number;
        let j: number;

        for (i = 0; i < polygon.length; i++) {
            for (j = 0; j < polygon[i].length; j++) {
                if (Points.equals(polygon[i][j], point)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Whether segment is equal to one of polygon segments.
     */
    public static hasSegment(polygon: Polygon, segment: Segment): boolean {
        let segmentIsPolygonSide: boolean = false;
        let i: number = 0;

        do {
            segmentIsPolygonSide = Rings.hasSegment(polygon[i], segment);
        } while (!segmentIsPolygonSide && (++i < polygon.length));

        return segmentIsPolygonSide;
    }

    public static segmentCrossPolygon(segment: Segment, polygon: Polygon): boolean {
        let cross: boolean = false;
        let i: number = 0;

        do {
            cross = Rings.segmentCrossRing(segment, polygon[i]);
        } while (!cross && (++i < polygon.length));

        return cross;
    }

    public static segments(polygon: Polygon): Segment[] {
        let segments: Segment[] = [];

        polygon
            .forEach((ring: Ring) => {
                segments = segments.concat(Rings.segments(ring));
            })
        ;

        return segments;
    }

    /**
     * Whether a point is inside polygon.
     * Beware with points exactly on a polygon side or point.
     */
    public static containsPoint(polygon: Polygon, point: Point): boolean {
        let i: number = 0;
        let inside: boolean;

        do {
            inside = (i === 0) === pointInPolygon(
                Points.toArray(point),
                polygon[i].map(Points.toArray),
            );
        } while (inside && (++i < polygon.length));

        return inside;
    }

    public static barycenter(polygon: Polygon): Point {
        const barycenter: Point = {x: 0, y: 0};

        polygon[0].forEach(polygonPoint => {
            barycenter.x += polygonPoint.x;
            barycenter.y += polygonPoint.y;
        });

        barycenter.x /= polygon[0].length;
        barycenter.y /= polygon[0].length;

        return barycenter;
    }

    /**
     * Get the nearset point inside polygon from originalPoint.
     * I.e, if original point is outside polygon,
     * returns a point on polygon border that is the nearest from originalPoint.
     */
    public static nearestPointInsidePolygon(originalPoint: Point, polygon: Polygon): Point {
        if (Polygons.containsPoint(polygon, originalPoint)) {
            return originalPoint;
        }

        const sideDistances = Polygons.segments(polygon)
            // Calculate distance from point to sides
            .map(side => ({
                side,
                distance: Lines.pointDistance(originalPoint, side),
            }))
            // Sort polygon side by nearest disance to point
            .sort((a, b) => a.distance > b.distance ? 1 : -1)
        ;

        const nearestSide = sideDistances[0].side;

        return Segments.nearestPoint(nearestSide, originalPoint);
    }

    /**
     * Returns a new Polygon, same as first but smallest by "padding".
     * Attention: input polygon expected to be in clockwise direction.
     *
     * @param Polygon polygon
     * @param number padding
     */
    public static padding(polygon: Polygon, padding: number): Polygon {
        let paddedPolygon: Polygon = [
            Rings.padding(polygon[0], padding, true),
        ];

        let i: number;

        for (i = 1; i < polygon.length; i++) {
            const inflatedHoleRing: Ring = Rings.padding(Rings.clone(polygon[i]).reverse(), -padding, true);
            paddedPolygon = Polygons.difference(paddedPolygon, [inflatedHoleRing]);
        }

        return paddedPolygon;
    }

    public static getConcavePoints(polygon: Polygon): Point[] {
        const concavePoints: Point[] = [];

        polygon.forEach((ring: Ring) => {
            for (let i = 1; i <= ring.length; i++) {
                const isPointOnRight = Lines.isPointOnRight(
                    [ring[i - 1], ring[i % ring.length]],
                    ring[(i + 1) % ring.length],
                );

                if (!isPointOnRight) {
                    concavePoints.push(ring[i % ring.length]);
                }
            }
        });

        return concavePoints;
    }

    public static segmentInsidePolygon(polygon: Polygon, segment: Segment): boolean {
        // Check if segment is actually a polygon side
        if (Polygons.hasSegment(polygon, segment)) {
            return true;
        }

        // Check segment is not crossing any polygon side
        if (Polygons.segmentCrossPolygon(segment, polygon)) {
            return false;
        }

        // Check segment is not going outside from a polygon point
        let ringIndex: number;

        for (ringIndex = 0; ringIndex < polygon.length; ringIndex++) {
            const ring: Ring = polygon[ringIndex];
            const pointIndex: number = Points.indexOfPoint(ring, segment[0]);

            if (pointIndex < 0) {
                continue;
            }

            const pointPrevious = ring[(pointIndex - 1 + ring.length) % ring.length];
            const pointCurrent = ring[pointIndex];
            const pointNext = ring[(pointIndex + 1) % ring.length];

            // Concave point is on acutes angles inside polygon (not acute inside holes)
            const isPointConcave = !Lines.isPointOnRight([pointPrevious, pointCurrent], pointNext, true);
            const isPointRightPreviousSide = Lines.isPointOnRight([pointPrevious, pointCurrent], segment[1], true);
            const isPointRightNextSide = Lines.isPointOnRight([pointCurrent, pointNext], segment[1], true);

            const segmentInsidePolygon: boolean = isPointConcave
                ? isPointRightPreviousSide || isPointRightNextSide
                : isPointRightPreviousSide && isPointRightNextSide
            ;

            if (!segmentInsidePolygon) {
                return false;
            }
        }

        // Check if first point is inside polygon at least
        if (!Polygons.hasPoint(polygon, segment[0]) && !Polygons.containsPoint(polygon, segment[0])) {
            return false;
        }

        return true;
    }

    public static difference(polygon: Polygon, ...obstacles: Polygon[]): Polygon {
        const difference: MultiPolygon = polygonDifference(
            Polygons.pointsToArray(polygon),
            ...obstacles.map(Polygons.pointsToArray),
        );

        if (difference.length > 1) {
            throw new Error('Array difference generated polygon splitted into more polygons.');
        }

        return Polygons.arrayPointsToPoints(difference[0]
            // Remove closing points
            .map(path => path.slice(0, path.length - 1)),
        );
    }

    public static union(...polygons: Polygon[]): Polygon {
        const arrayPointsPolygons: ArrayPointsPolygon[] = polygons.map(Polygons.pointsToArray);
        const arrayPointsPolygon0 = arrayPointsPolygons.shift();
        const unionMultiPolygon: MultiPolygon = polygonUnion(arrayPointsPolygon0, ...arrayPointsPolygons);

        if (unionMultiPolygon.length > 1) {
            throw new Error('Union of polygon generated a multipolygon.');
        }

        return Polygons.arrayPointsToPoints(unionMultiPolygon[0]
            // Remove closing points
            .map(path => path.slice(0, path.length - 1)),
        );
    }
}
