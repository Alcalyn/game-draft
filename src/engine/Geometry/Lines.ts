import 'engine/Geometry/Types';
import Points from 'engine/Geometry/Points';

export default class Lines {

    public static pointDistance(point: Point, line: Line): number {
        const a = point.x - line[0].x;
        const b = point.y - line[0].y;
        const c = line[1].x - line[0].x;
        const d = line[1].y - line[0].y;

        const dot = a * c + b * d;
        const lenSq = c * c + d * d;
        let param = -1;

        if (lenSq !== 0) {
            param = dot / lenSq;
        }

        let xx;
        let yy;

        if (param < 0) {
            xx = line[0].x;
            yy = line[0].y;
        } else if (param > 1) {
            xx = line[1].x;
            yy = line[1].y;
        } else {
            xx = line[0].x + param * c;
            yy = line[0].y + param * d;
        }

        const dx = point.x - xx;
        const dy = point.y - yy;

        return Math.hypot(dx, dy);
    }

    /**
     * Whether is a point in at right of a line
     * in a SCREEN COORDINATES system (x: left to right, y: top to bottom).
     */
    public static isPointOnRight(line: Line, p: Point, inclusive: boolean = false): boolean {
        const l1Origin: Point = {x: line[1].x - line[0].x, y: line[1].y - line[0].y};
        const pOrigin: Point = {x: p.x - line[0].x, y: p.y - line[0].y};
        const crossProduct: number = Points.crossProduct(l1Origin, pOrigin);

        return inclusive ? crossProduct >= 0 : crossProduct > 0;
    }
}
