import 'engine/Geometry/Types';
import Points from 'engine/Geometry/Points';
import Lines from 'engine/Geometry/Lines';

export default class Segments {

    public static equals(segmentA: Segment, segmentB: Segment): boolean {
        if (Points.equals(segmentA[0], segmentB[0]) && Points.equals(segmentA[1], segmentB[1])) {
            return true;
        }

        if (Points.equals(segmentA[0], segmentB[1]) && Points.equals(segmentA[1], segmentB[0])) {
            return true;
        }

        return false;
    }

    public static nearestPoint(segment: Segment, point: Point): Point {
        const ap = {x: point.x - segment[0].x, y: point.y - segment[0].y};
        const ab = {x: segment[1].x - segment[0].x, y: segment[1].y - segment[0].y};

        const ab2 = ab.x * ab.x + ab.y * ab.y;
        const apab = ap.x * ab.x + ap.y * ab.y;
        let t = apab / ab2;

        // stay on segment
        t = Math.max(0, t);
        t = Math.min(1, t);

        return {x: segment[0].x + ab.x * t, y: segment[0].y + ab.y * t};
    }

    /**
     * Returns whether point is above the segment,
     * where segment is extended before and after.
     */
    public static isPointAbove(segment: Segment, point: Point): boolean {
        const segmentVector: Vector = Points.difference(segment[1], segment[0]);
        const pointVecor: Vector = Points.difference(point, segment[0]);

        return pointVecor.x * segmentVector.y / segmentVector.x < pointVecor.y;
    }

    public static crossLine(segment: Segment, line: Line): boolean {
        return Lines.isPointOnRight(line, segment[0], true)
            !== Lines.isPointOnRight(line, segment[1], true)
        ;
    }

    public static boundingBoxesIntersect(a: Segment, b: Segment): boolean {
        const { min, max } = Math;

        const minA = {x: min(a[0].x, a[1].x), y: min(a[0].y, a[1].y)};
        const maxA = {x: max(a[0].x, a[1].x), y: max(a[0].y, a[1].y)};
        const minB = {x: min(b[0].x, b[1].x), y: min(b[0].y, b[1].y)};
        const maxB = {x: max(b[0].x, b[1].x), y: max(b[0].y, b[1].y)};

        return minA.x < maxB.x
            && maxA.x > minB.x
            && minA.y < maxB.y
            && maxA.y > minB.y
        ;
    }

    public static cross(a: Segment, b: Segment): boolean {
        return Segments.boundingBoxesIntersect(a, b)
            && Segments.crossLine(a, b)
            && Segments.crossLine(b, a)
        ;
    }
}
