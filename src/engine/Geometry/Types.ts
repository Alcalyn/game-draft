type Point = {x: number, y: number};
type Segment = [Point, Point];
type Line = [Point, Point];
type Path = Point[];
type Ring = Point[];

/**
 * Polygon follows this rule of the RFC 7946, paragraph "3.1.6. Polygon":
 *
 * "A linear ring MUST follow the right-hand rule with respect to the
 * area it bounds, i.e., exterior rings are counterclockwise, and
 * holes are clockwise."
 *
 * So related to screen coordinates where y axis is reverted,
 * it means that exterior ring is clockwise (on screen),
 * and holes are counter clockwise.
 *
 * Closing point is NOT required.
 */
type Polygon = Ring[];

type Vector = Point;
