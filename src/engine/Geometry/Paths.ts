import 'engine/Geometry/Types';
import Points from 'engine/Geometry/Points';
import Segments from './Segments';

export default class Paths {

    public static clone(path: Path): Path {
        return path.map(point => ({
            x: point.x,
            y: point.y,
        }));
    }

    public static segments(path: Path): Segment[] {
        const segments: Segment[] = [];
        let i: number;

        for (i = 1; i < path.length; i++) {
            segments.push([
                path[i - 1],
                path[i],
            ]);
        }

        return segments;
    }

    /**
     * Returns whether point is above path,
     * i.e point.y is higher than path segment y at this position.
     *
     * If point is inside path, extends the nearest segment.
     */
    public static isPointAbove(path: Path, point: Point): boolean {
        if (path.length < 2) {
            throw new Error('Calling isPointAbove with a 0 or 1 point path');
        }

        if (2 === path.length) {
            return Segments.isPointAbove(path as Segment, point);
        }

        // Makes sure path goes from left to right
        if (path[path.length - 1].x < path[0].x) {
            path = Paths.clone(path).reverse();
        }

        let i: number = 1;

        while (
            (point.x > path[i].x)
            && (++i < path.length - 1)
        );

        const segment: Segment = [path[i - 1], path[i]];

        return Segments.isPointAbove(segment, point);
    }
}
