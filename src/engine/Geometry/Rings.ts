import Offset from 'polygon-offset';
import Points from 'engine/Geometry/Points';
import Segments from 'engine/Geometry/Segments';

export default class Rings {

    public static clone(ring: Ring): Ring {
        return ring.map((point: Point) => ({x: point.x, y: point.y}));
    }

    public static segments(ring: Ring): Segment[] {
        return ring.map((point, i, points): Segment => [
            point,
            points[(i + 1) % points.length],
        ]);
    }

    /**
     * Whether segment is equal to one of ring segments
     */
    public static hasSegment(ring: Ring, segment: Segment): boolean {
        const ringSegments: Segment[] = Rings.segments(ring);
        let contains: boolean = false;
        let i: number = 0;

        do {
            contains = Segments.equals(segment, ringSegments[i]);
        } while (!contains && (++i < ringSegments.length));

        return contains;
    }

    public static segmentCrossRing(segment: Segment, ring: Ring): boolean {
        const ringSegments: Segment[] = Rings.segments(ring);
        let cross: boolean = false;
        let i: number = 0;

        do {
            cross = Segments.cross(segment, ringSegments[i]);
        } while (!cross && (++i < ringSegments.length));

        return cross;
    }

    public static padding(ring: Ring, padding: number, clockwise: boolean = true): Ring {
        if (!Points.equals(ring[0], ring[ring.length - 1])) {
            ring = Rings.clone(ring);
            ring.push(ring[0]);
        }

        const paddedRing: Ring = (new Offset())
            .data(ring.map(Points.toArray))     // Use array points
            .arcSegments(3)                     // Only three points in rounded concave corners
            .offset(-padding)                   // Perform padding
            .pop()                              // Take first array as api is returning an array of array
            .slice(0, -1)                       // Remove last closing point
            .map((pair: [number, number]) => pair.map(Math.round))              // Round points coordinates
            .map((pair: [number, number]): Point => ({x: pair[0], y: pair[1]})) // Create types of Point
        ;

        // Reorder ring points in clockwise direction as api is returning anticlockwise
        if (clockwise) {
            const firstPoint: Point = paddedRing.shift();
            paddedRing.reverse();
            paddedRing.unshift(firstPoint);
        }

        return paddedRing;
    }
}
