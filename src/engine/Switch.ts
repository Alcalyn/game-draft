import { EventEmitter } from 'events';

/**
 * Event names used for any object that can be switched,
 * i.e open and closed like doors, room connections...
 *
 * @event Switch#open
 * @event Switch#closed
 * @event Switch#switched
 */
export default class Switch {
    /**
     * Item is now open.
     *
     * @event Switch#open
     */
    public static readonly OPEN = 'open';

    /**
     * Item is now closed.
     *
     * @event Switch#closed
     */
    public static readonly CLOSED = 'closed';

    /**
     * Item state has changed from open to closed
     * or from closed to open
     *
     * @event Switch#switched
     */
    public static readonly SWITCHED = 'switched';

    public static emit(emitter: EventEmitter | PIXI.utils.EventEmitter, state: 'open' | 'closed'): void {
        emitter.emit(state);
        emitter.emit(Switch.SWITCHED);
    }
}
