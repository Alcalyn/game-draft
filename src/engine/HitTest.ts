import Actor from './Scene/Actor/Actor';
import Polygons from './Geometry/Polygons';
import Points from './Geometry/Points';

export default class HitTest {
    /**
     * Whether a point actor touches an object polygon,
     * or if no polygon, is near enough depending on actors radiuses.
     */
    public static actorIsInside(pointActor: Actor, objectActor: Actor): boolean {
        if (objectActor.hasPolygon()) {
            return Polygons.containsPoint(objectActor.getPolygonToScene(), pointActor);
        }

        return Points.distance([pointActor, objectActor]) <= (pointActor.getRadius() + objectActor.getRadius());
    }
}
