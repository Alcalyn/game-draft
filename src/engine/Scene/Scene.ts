import { Container } from 'pixi.js';
import Polygons from 'engine/Geometry/Polygons';
import Character from 'engine/Character/Character';
import RestrictedArea from 'engine/Scene/Room/RestrictedArea';
import ActorSlot from 'engine/Scene/Actor/ActorSlot';
import Actor from 'engine/Scene/Actor/Actor';
import RoomConnection from 'engine/Scene/Room/RoomConnection';
import ActorContainer from 'engine/Scene/Actor/ActorContainer';
import Room from 'engine/Scene/Room/Room';
import Switch from 'engine/Switch';

export default class Scene extends Container {

    private rooms: {[key: string]: Room} = {};

    private roomConnections: RoomConnection[] = [];

    private characters: Character[] = [];

    /**
     * Contains sprites that draw scene background.
     */
    public readonly background = new Container();

    /**
     * Actors still behind others actors,
     * i.e items on floor, invisible areas...
     */
    public readonly floorActors = new ActorContainer();

    /**
     * Scene actors that can be in front/back of other actors
     * (i.e Characters, obstacles, scene walls or doors...).
     *
     * Must not be transformed or Actor coordinates will be broken.
     */
    public readonly actors = new ActorContainer(true);

    /**
     * Contains sprites that should be display in front of actors,
     * i.e dialogues.
     */
    public readonly foreground = new Container();

    public constructor() {
        super();

        this.addChild(
            this.background,
            this.floorActors,
            this.actors,
            this.foreground,
        );

        const slotChangedCallback = () => {
            this.updateCharactersRestrictedArea();
        };

        [
            this.floorActors,
            this.actors,
        ].forEach(actors => actors.on('childAdded', (actor: Actor) => {
            actor.setScene(this);

            if (actor instanceof ActorSlot) {
                actor.on(ActorSlot.SLOT_CHANGED, slotChangedCallback);
            }

            if (actor instanceof Character) {
                actor.stopActions();

                this.characters.push(actor);

                if (null === this.getCharacterRoom(actor)) {
                    throw new Error(
                        `Character at (${actor.position.x}, ${actor.position.y})
                        is not on any room of the scene.`,
                    );
                }

                this.updateCharacterRestrictedArea(actor);
            }
        }));

        this.actors.on('childRemoved', (actor: Actor) => {
            actor.setScene(null);

            if (actor instanceof ActorSlot) {
                actor.off(ActorSlot.SLOT_CHANGED, slotChangedCallback);
            }

            if (actor instanceof Character) {
                actor.stopActions();

                const index = this.characters.indexOf(actor);
                if (index >= 0) {
                    this.characters.splice(index, 1);
                }

                actor.setRestrictedArea(null);
            }
        });
    }

    public getActor(name: string): Actor {
        return this.actors.getChildByName(name) as Actor
            || this.floorActors.getChildByName(name) as Actor
        ;
    }

    public getObstacles(): Actor[] {
        return this.actors.children
            .filter(actor => actor.isObstacle())
        ;
    }

    public getRoom(name: string): Room {
        if (!this.rooms[name]) {
            return null;
        }

        return this.rooms[name];
    }

    public addRoom(name: string, room: Room): Scene {
        this.rooms[name] = room;

        return this;
    }

    public addRoomConnection(roomConnection: RoomConnection): Scene {
        this.roomConnections.push(roomConnection);

        roomConnection.on(Switch.SWITCHED, () => {
            this.updateCharactersRestrictedArea();
        });

        return this;
    }

    public getRoomConnection(roomNames: string[]): RoomConnection {
        const rooms = roomNames.map(name => {
            if (!this.rooms[name]) {
                throw new Error(`Scene has no room with name "${name}"`);
            }

            return this.rooms[name];
        });

        return this.roomConnections
            .find(roomConnection => {
                if (roomConnection.getRooms().length !== rooms.length) {
                    return false;
                }

                let i: number = 0;

                for (i = 0; i < rooms.length; i++) {
                    if (!roomConnection.getRooms().includes(rooms[i])) {
                        return false;
                    }
                }

                return true;
            })
        ;
    }

    public getRoomAt(position: Point): Room {
        const rooms = Object.values(this.rooms);
        let room: Room = null;
        let i: number = 0;

        while (null === room && i < rooms.length) {
            if (Polygons.containsPoint(rooms[i].getArea(), position)) {
                room = rooms[i];
            }

            i++;
        }

        return room;
    }

    public getCharacterRoom(character: Character): Room {
        return this.getRoomAt(character.position);
    }

    public updateCharacterRestrictedArea(character: Character): void {
        const restrictedArea = this.createRestrictedAreaAt(character.position, character.getRadius());
        character.setRestrictedArea(restrictedArea);
    }

    public updateCharactersRestrictedArea(): void {
        this.characters.forEach(character => this.updateCharacterRestrictedArea(character));
    }

    public createRestrictedAreaAt(position: Point, padding: number = 0): RestrictedArea {
        const room = this.getRoomAt(position);

        if (null === room) {
            throw new Error('Cannot create RestrictedArea from outside rooms');
        }

        // Get union of room areas where doors are open
        let area: Polygon = Polygons.union(
            ...this
                .getConnexeRooms(room)
                .map(connexeRoom => connexeRoom.getArea())
            ,
        );

        // Remove obstacles
        this.getObstacles()
            .filter(obstacle => obstacle.isObstacle())
            .map(obstacle => obstacle.getPolygonToScene())
            .forEach(polygon => {
                area = Polygons.difference(area, polygon);
            })
        ;

        const restrictedArea = new RestrictedArea(area, padding);

        return restrictedArea;
    }

    public getConnexeRooms(fromRoom: Room): Room[] {
        let openConnections = this.roomConnections
            .filter(connection => connection.isOpen())
        ;

        const connexeRooms = [fromRoom];
        let i = 0;

        do {
            openConnections = openConnections.filter(connection => {
                let keepConnection = true;

                if (connection.getRooms().includes(connexeRooms[i])) {
                    connection.getRooms()
                        .filter(room => !connexeRooms.includes(room))
                        .forEach(room => connexeRooms.push(room))
                    ;

                    keepConnection = false;
                }

                return keepConnection;
            });
        } while (++i < connexeRooms.length);

        return connexeRooms;
    }
}
