import ActorSlot from './ActorSlot';
import Actor from './Actor';
import Switch from 'engine/Switch';

/**
 * Basic ActorSlot with two slots:
 * one for open state and one for closed state.
 *
 * Provides an API to open and close the door.
 *
 * Useful for a door Actor.
 */
export default class Door extends ActorSlot {
    constructor(openActor: Actor, closedActor: Actor, open: boolean = false) {
        super({
            [Switch.OPEN]: openActor,
            [Switch.CLOSED]: closedActor,
        });

        this.setOpen(open);
    }

    public isOpen(): boolean {
        return this.getCurrentActorName() === Switch.OPEN;
    }

    public setOpen(open: boolean): this {
        return this.display(open ? Switch.OPEN : Switch.CLOSED);
    }

    public open(): this {
        this.display(Switch.OPEN);
        Switch.emit(this, Switch.OPEN);

        return this;
    }

    public close(): this {
        this.display(Switch.CLOSED);
        Switch.emit(this, Switch.CLOSED);

        return this;
    }

    public toggle(): this {
        if (this.isOpen()) {
            return this.close();
        }

        return this.open();
    }
}
