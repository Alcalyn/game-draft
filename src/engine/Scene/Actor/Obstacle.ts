import 'engine/Geometry/Types';
import Actor from 'engine/Scene/Actor/Actor';

/**
 * Simple actor that behaves like an obstacle.
 */
export default class Obstacle extends Actor {

    public constructor(texture?: PIXI.Texture, polygon?: Polygon) {
        super(texture);

        this.setObstacle(true, polygon);
    }
}
