import { Point as PixiPoint } from 'pixi.js';
import { Sprite } from 'pixi.js';
import Scene from '../Scene';

/**
 * Object on the scene that can be in front of/back of other objects.
 */
export default class Actor extends Sprite {

    /**
     * The scene where the actor is currently on.
     * The actor cannot be on multiple scene at same time.
     */
    private scene: Scene = null;

    /**
     * How this actor is shaped.
     *
     * If null, this actor behaves like a point,
     * so rely on y coordinate to know about zIndex.
     *
     * If set and isObstacle is true,
     * the polygon represents an obstacle in the scene.
     */
    private polygon: Polygon = null;

    /**
     * Whether the polygon of this actor
     * represents an obstacle.
     */
    private obstacle: boolean = false;

    /**
     * Radius around the Actor position if no obstacle polygon.
     */
    private radius: number = 0;

    /**
     * Front line of the actor to know
     * if a point is in front of or behind this actor.
     *
     * Path should go to a horizontal direction, but not go back.
     *
     * The shape can be deducted from the bottom line of polygon.
     */
    private shape: Path = null;

    public getScene(): Scene {
        return this.scene;
    }

    public setScene(scene: Scene = null): this {
        this.scene = scene;

        return this;
    }

    public hasPolygon(): boolean {
        return null !== this.polygon;
    }

    public getPolygon(): Polygon {
        return this.polygon;
    }

    public setPolygon(polygon: Polygon): Actor {
        this.polygon = polygon;

        return this;
    }

    public isObstacle(): boolean {
        return this.obstacle && null !== this.polygon;
    }

    public setObstacle(obstacle: boolean, polygon?: Polygon): Actor {
        if (obstacle && !polygon && null === this.polygon) {
            throw new Error('You must specify a polygon for this obstacle actor');
        }

        this.obstacle = obstacle;

        if (polygon) {
            this.setPolygon(polygon);
        }

        return this;
    }

    public hasRadius(): boolean {
        return this.radius > 0;
    }

    public getRadius(): number {
        return this.radius;
    }

    public setRadius(radius: number): this {
        this.radius = radius;

        return this;
    }

    public getShape(): Path {
        return this.shape;
    }

    public setShape(shape: Path): Actor {
        this.shape = shape;

        return this;
    }

    public hasShape(): boolean {
        return null !== this.getShape();
    }

    public getPointToScene(point: Point): Point {
        if (null === this.scene) {
            throw new Error('Cannot translate actor coordinate to scene because actor is not on any scene');
        }

        return this.scene.toLocal(new PixiPoint(point.x, point.y), this);
    }

    public getPathToScene(path: Path): Path {
        return path.map(point => {
            return this.getPointToScene(point);
        });
    }

    /**
     * Get polygon translated to scene.
     *
     * @param {Actor} actor The actor that contains the polygon
     * @param {Polygon} polygon The polygon. If not set, returns the actor polygon.
     *                          Can be used to translate the actor shape instead.
     */
    public getPolygonToScene(polygon: Polygon = null): Polygon {
        if (null === polygon) {
            polygon = this.getPolygon();
        }

        return polygon.map(ring => {
            return this.getPathToScene(ring);
        });
    }
}
