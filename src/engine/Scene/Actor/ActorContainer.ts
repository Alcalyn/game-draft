import { Container, DisplayObject, Point as PixiPoint } from 'pixi.js';
import Paths from 'engine/Geometry/Paths';
import Actor from 'engine/Scene/Actor/Actor';

/**
 * A container of actors that can be ordered
 * with their zIndex.
 */
export default class ActorContainer extends Container {

    /**
     * {@inheritdoc}
     *
     * @member {Actor[]} ActorContainer#children
     */
    public readonly children: Actor[];

    public constructor(sortChildren: boolean = false) {
        super();

        this.sortableChildren = sortChildren;

        this.on('childAdded', (child: DisplayObject) => {
            if (!(child instanceof Actor)) {
                throw new Error('Trying to add a non Actor to an ActorContainer');
            }
        });
    }

    /**
     * Override Container sort function
     * to sort Actors by their positions and shapes.
     */
    public sortChildren(): void {
        // Take shaped actors and insert point like actors inside
        const sortedActors: Actor[] = [];
        const pointActors: Actor[] = [];

        this.children.forEach(actor => {
            (actor.hasShape() ? sortedActors : pointActors).push(actor);
        });

        pointActors.forEach(pointActor => {
            let i: number = 0;
            let actorStillInFront: boolean = true;

            // Sort point actor in shapes
            while (actorStillInFront && i < sortedActors.length) {
                if (sortedActors[i].hasShape()) {
                    actorStillInFront = Paths.isPointAbove(this.actorShapeToLocal(sortedActors[i]), pointActor);
                }

                if (actorStillInFront) {
                    i++;
                }
            }

            // Sort local point actors by their y
            while (
                i > 0
                && !sortedActors[i - 1].hasShape()
                && pointActor.y < sortedActors[i - 1].y
            ) --i;

            // Insert point actor
            sortedActors.splice(i, 0, pointActor);
        });

        sortedActors.forEach((actor, index) => actor.zIndex = index);

        super.sortChildren();
    }

    private getActorPointToLocal(actor: Actor, point: Point): Point {
        return this.toLocal(new PixiPoint(point.x, point.y), actor);
    }

    private getActorPathToLocal(actor: Actor, path: Path): Path {
        return path.map(point => {
            return this.getActorPointToLocal(actor, point);
        });
    }

    private actorShapeToLocal(actor: Actor): Path {
        return this.getActorPathToLocal(actor, actor.getShape());
    }
}
