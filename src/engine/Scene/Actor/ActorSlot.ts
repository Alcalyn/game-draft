import Actor from 'engine/Scene/Actor/Actor';

/**
 * An actor containing a slot of actors and can be switched
 * to any actors from this slot.
 * Can be an obstacle or not depending on the current displaying actor.
 *
 * @fires ActorSlot#slot.changed
 */
export default class ActorSlot extends Actor {
    /**
     * When another actor of the slot is now displayed.
     *
     * @event ActorSlot#slot.changed
     */
    public static readonly SLOT_CHANGED = 'slot.changed';

    /**
     * List of actors this slot can switch to.
     * Can have Obstacle or just Actor,
     * in this last case, this actors slot
     * is no longer an obstacle until switched again.
     */
    private actors: {[key: string]: Actor} = {};

    private currentActorName: string = null;

    public constructor(actors: {[key: string]: Actor}) {
        super();

        const keys = Object.keys(actors);

        if (0 === keys.length) {
            throw new Error('Trying to create a ActorSlot with empty actors.');
        }

        keys.forEach(key => {
            actors[key].visible = false;
            this.addChild(actors[key]);
        });

        this.actors = actors;

        this.display(keys[0]);
    }

    public display(name: string): this {
        if (name === this.currentActorName) {
            return;
        }

        if (null !== this.currentActorName) {
            this.getCurrentActor().visible = false;
        }

        this.actors[name].visible = true;
        this.currentActorName = name;

        this.emit(ActorSlot.SLOT_CHANGED);

        return this;
    }

    public getCurrentActorName(): string {
        return this.currentActorName;
    }

    private getCurrentActor(): Actor {
        return this.actors[this.currentActorName];
    }

    /**
     * {@inheritdoc}
     */
    public getPolygon(): Polygon {
        return this.getCurrentActor().getPolygon();
    }

    /**
     * {@inheritdoc}
     */
    public setPolygon(): Actor {
        throw new Error('Cannot modify a slot. Modify items instead.');
    }

    /**
     * {@inheritdoc}
     */
    public isObstacle(): boolean {
        return this.getCurrentActor().isObstacle();
    }

    /**
     * {@inheritdoc}
     */
    public setObstacle(): Actor {
        throw new Error('Cannot modify a slot. Modify items instead.');
    }

    /**
     * {@inheritdoc}
     */
    public getShape(): Path {
        return this.getCurrentActor().getShape();
    }

    /**
     * {@inheritdoc}
     */
    public setShape(): Actor {
        throw new Error('Cannot modify a slot. Modify items instead.');
    }

    /**
     * {@inheritdoc}
     */
    public hasShape(): boolean {
        return this.getCurrentActor().hasShape();
    }
}
