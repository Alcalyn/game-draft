import 'engine/Geometry/Types';

export default class Room {

    /**
     * Room area.
     *
     * Must be set clockwise for screen coordinates.
     */
    protected area: Polygon;

    public constructor(area?: Polygon) {
        this.area = area;
    }

    public setArea(area: Polygon): Room {
        this.area = area;

        return this;
    }

    public getArea(): Polygon {
        return this.area;
    }
}
