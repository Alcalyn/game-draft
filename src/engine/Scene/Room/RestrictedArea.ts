import 'engine/Geometry/Types';
import { Graph } from 'dijkstra-pathfinder';
import Polygons from 'engine/Geometry/Polygons';
import PathFinder from 'engine/PathFinder';

/**
 * A processed area in which a character can move
 * and cannot go outside.
 *
 * The area depends on the room where the character is,
 * whether the room has open doors to other rooms,
 * obstacles in the room...
 */
export default class RestrictedArea {

    private area: Polygon;

    /**
     * Padded area with a thin margin
     * used to prevent character walk on the border
     * of the polygon which can be buggy with pathfinder.
     *
     * This inner area is used to get a nearest point
     * of an outside point in the area.
     */
    private innerArea: Polygon;

    /**
     * Pre-initialized graph used for Dijkstra algorithm
     * to find the shortest path when a Character moves inside this room.
     */
    private preInitializedDijkstraGraph: Graph;

    public constructor(area: Polygon, padding: number = 0) {
        this.area = area;

        if (0 !== padding) {
            this.area = Polygons.padding(area, padding);
        }

        this.innerArea = Polygons.padding(area, padding + 1);

        this.preInitializedDijkstraGraph = PathFinder.createDijkstraGraphFromPolygon(this.area);
    }

    public getArea(): Polygon {
        return this.area;
    }

    public getInnerArea(): Polygon {
        return this.innerArea;
    }

    public createPreInitializedDijkstraGraph(): Graph {
        return this.preInitializedDijkstraGraph.clone();
    }
}
