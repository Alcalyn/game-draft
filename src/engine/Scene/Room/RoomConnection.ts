import { EventEmitter } from 'events';
import Room from 'engine/Scene/Room/Room';
import Switch from 'engine/Switch';

/**
 * @fires Switch#open
 * @fires Switch#closed
 * @fires Switch#switched
 */
export default class RoomConnection extends EventEmitter {
    /**
     * Rooms joined by this door
     */
    private rooms: Room[];

    private connectionOpen: boolean;

    public constructor(rooms: Room[], open: boolean = false) {
        super();

        this.rooms = rooms;
        this.connectionOpen = open;
    }

    public isOpen(): boolean {
        return this.connectionOpen;
    }

    public open(): RoomConnection {
        return this.setOpen(true);
    }

    public close(): RoomConnection {
        return this.setOpen(false);
    }

    public toggle(): RoomConnection {
        return this.setOpen(!this.connectionOpen);
    }

    public setOpen(open: boolean): RoomConnection {
        if (open === this.connectionOpen) {
            return this;
        }

        this.connectionOpen = open;

        Switch.emit(this, this.open ? Switch.OPEN : Switch.CLOSED);

        return this;
    }

    public getRooms(): Room[] {
        return this.rooms;
    }
}
