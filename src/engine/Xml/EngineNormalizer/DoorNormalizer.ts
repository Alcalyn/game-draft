import ActorSlotNormalizer from './ActorSlotNormalizer';
import Door from 'engine/Scene/Actor/Door';
import NormalizerCollection from '../XmlSerializer/NormalizerCollection';
import { Element } from '../XmlSerializer/Element';

export default class DoorNormalizer extends ActorSlotNormalizer {
    public fromJson(doorElement: Element, normalizers: NormalizerCollection): Door {
        const actors = this.getActors(doorElement, normalizers);

        if (Object.entries(actors).length !== 2 || !actors.open || !actors.closed) {
            throw new Error([
                'Door element must have exactly 2 element.',
                'One with key="open", one with key="closed".',
                `Got ${Object.entries(actors).length} elements.`,
            ].join(' '));
        }

        const door = new Door(actors.open, actors.closed);

        this.initializeActorSlot(door, doorElement);

        door.setOpen('true' === doorElement.attributes.open);

        return door;
    }
}
