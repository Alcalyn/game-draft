import { Texture } from 'pixi.js';
import NormalizerInterface from '../XmlSerializer/NormalizerInterface';
import NormalizerCollection from '../XmlSerializer/NormalizerCollection';
import { Element } from '../XmlSerializer/Element';

export default class TextureNormalizer implements NormalizerInterface<Texture> {
    public fromJson(textureElement: Element, normalizers: NormalizerCollection): Texture {
        const texture = Texture.from(textureElement.attributes.src);

        textureElement.elements.forEach(element => {
            switch (element.name) {
                case 'Anchor':
                    const anchor = normalizers
                        .getNormalizer<Point>('Point')
                        .fromJson(element, normalizers)
                    ;

                    texture.defaultAnchor.set(anchor.x, anchor.y);
                    break;

                default:
                    throw new Error(`Unexpected Texture tag: "${element.name}"`);
            }
        });

        return texture;
    }
}
