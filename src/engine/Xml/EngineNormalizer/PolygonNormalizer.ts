import NormalizerInterface from '../XmlSerializer/NormalizerInterface';
import { Element } from '../XmlSerializer/Element';
import NormalizerCollection from '../XmlSerializer/NormalizerCollection';

export default class PolygonNormalizer implements NormalizerInterface<Polygon> {
    public fromJson(json: Element, normalizers: NormalizerCollection): Polygon {
        const ringNormalizer = normalizers.getNormalizer<Ring>('Ring');

        return json
            .elements
            .map(ringElement => ringNormalizer.fromJson(ringElement, normalizers))
        ;
    }
}
