import NormalizerInterface from '../XmlSerializer/NormalizerInterface';
import NormalizerCollection from '../XmlSerializer/NormalizerCollection';
import { Element } from '../XmlSerializer/Element';

export default class PathNormalizer implements NormalizerInterface<Path> {
    public fromJson(pathElement: Element, normalizers: NormalizerCollection): Path {
        return pathElement.elements.map(pointElement => {
            return normalizers.fromJson<Point>(pointElement);
        });
    }
}
