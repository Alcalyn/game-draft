import { DisplayObject } from 'pixi.js';
import NormalizerInterface from '../XmlSerializer/NormalizerInterface';
import Scene from 'engine/Scene/Scene';
import NormalizerCollection from '../XmlSerializer/NormalizerCollection';
import Actor from 'engine/Scene/Actor/Actor';
import { Element } from '../XmlSerializer/Element';
import Room from 'engine/Scene/Room/Room';
import RoomConnection from 'engine/Scene/Room/RoomConnection';
import ActorContainer from 'engine/Scene/Actor/ActorContainer';

export default class SceneNormalizer implements NormalizerInterface<Scene> {
    public fromJson(sceneElement: Element, normalizers: NormalizerCollection): Scene {
        const scene = new Scene();

        this.addBackgrounds(scene, sceneElement, normalizers);
        this.addRooms(scene, sceneElement, normalizers);
        this.addActors('FloorActors', scene.floorActors, sceneElement, normalizers);
        this.addActors('Actors', scene.actors, sceneElement, normalizers);

        return scene;
    }

    private addBackgrounds(scene: Scene, sceneElement: Element, normalizers: NormalizerCollection): void {
        const backgrounds = sceneElement.elements.find(element => element.name === 'Backgrounds');

        if (backgrounds) {
            backgrounds.elements.forEach(backgroundElement => {
                const background = normalizers.fromJson<DisplayObject>(backgroundElement);
                scene.background.addChild(background);
            });
        }
    }

    private addRooms(scene: Scene, sceneElement: Element, normalizers: NormalizerCollection): void {
        const roomsElement = sceneElement.elements.find(element => element.name === 'Rooms');

        if (roomsElement) {
            roomsElement.elements
                .filter(element => 'Room' === element.name)
                .forEach(roomElement => {
                    const room = normalizers.fromJson<Room>(roomElement);
                    scene.addRoom(roomElement.attributes.name, room);
                })
            ;

            roomsElement.elements
                .filter(element => 'RoomConnection' === element.name)
                .forEach(roomConnectionElement => {
                    const rooms: Room[] = [];
                    let open: boolean = false;

                    roomConnectionElement.elements.forEach(element => {
                        rooms.push(scene.getRoom(element.attributes.to));
                    });

                    if (roomConnectionElement.attributes.open) {
                        open = 'true' === roomConnectionElement.attributes.open;
                    }

                    scene.addRoomConnection(new RoomConnection(rooms, open));
                })
            ;
        }
    }

    private addActors(
        tag: string,
        container: ActorContainer,
        sceneElement: Element,
        normalizers: NormalizerCollection,
    ): void {
        const actors = sceneElement.elements.find(element => element.name === tag);

        if (actors) {
            actors.elements.forEach(actorElement => {
                const actor = normalizers.fromJson<Actor>(actorElement);
                container.addChild(actor);
            });
        }
    }
}
