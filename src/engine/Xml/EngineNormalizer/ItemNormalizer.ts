import NormalizerCollection from '../XmlSerializer/NormalizerCollection';
import { Element } from '../XmlSerializer/Element';
import ActorNormalizer from './ActorNormalizer';
import Item from 'engine/Inventory/Item';

export default class ItemNormalizer extends ActorNormalizer {
    public fromJson(itemElement: Element, normalizers: NormalizerCollection): Item {
        const item = new Item(this.getElementTexture(itemElement, normalizers));

        this.initializeActor(item, itemElement, normalizers);

        return item;
    }
}
