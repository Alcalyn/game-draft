import NormalizerCollection from '../XmlSerializer/NormalizerCollection';
import { Element } from '../XmlSerializer/Element';
import ActorSlot from 'engine/Scene/Actor/ActorSlot';
import Actor from 'engine/Scene/Actor/Actor';
import SpriteNormalizer from './SpriteNormalizer';

export default class ActorSlotNormalizer extends SpriteNormalizer {
    public fromJson(actorSlotElement: Element, normalizers: NormalizerCollection): ActorSlot {
        const actorSlot = new ActorSlot(this.getActors(actorSlotElement, normalizers));

        this.initializeActorSlot(actorSlot, actorSlotElement);

        return actorSlot;
    }

    protected initializeActorSlot(
        actorSlot: ActorSlot,
        actorSlotElement: Element,
    ): void {
        this.initializeSprite(actorSlot, actorSlotElement);
    }

    protected getActors(actorSlotElement: Element, normalizers: NormalizerCollection): {[key: string]: Actor} {
        const actors: {[key: string]: Actor} = {};

        actorSlotElement.elements.forEach(actorElement => {
            actors[actorElement.attributes.key] = normalizers.fromJson(actorElement);
        });

        return actors;
    }
}
