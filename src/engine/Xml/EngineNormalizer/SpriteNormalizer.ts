import NormalizerInterface from '../XmlSerializer/NormalizerInterface';
import { Sprite } from 'pixi.js';
import { Element } from '../XmlSerializer/Element';
import NormalizerCollection from '../XmlSerializer/NormalizerCollection';

export default class SpriteNormalizer implements NormalizerInterface<Sprite> {

    // @ts-ignore: 'normalizers' is declared but its value is never read
    public fromJson(spriteElement: Element, normalizers: NormalizerCollection): Sprite {
        const sprite = Sprite.from(spriteElement.attributes.src);

        this.initializeSprite(sprite, spriteElement);

        return sprite;
    }

    protected initializeSprite(sprite: Sprite, spriteElement: Element): void {
        const {name, x, y, positionScale, alpha, scale} = spriteElement.attributes;

        if (name) {
            sprite.name = name;
        }

        if (x) {
            sprite.position.x = parseFloat(x);
        }

        if (y) {
            sprite.position.y = parseFloat(y);
        }

        if (positionScale) {
            const float = parseFloat(positionScale);

            sprite.position.set(
                sprite.position.x * float,
                sprite.position.y * float,
            );
        }

        if (scale) {
            sprite.scale.set(parseFloat(scale));
        }

        if (alpha) {
            sprite.alpha = parseFloat(alpha);
        }
    }
}
