import NormalizerInterface from '../XmlSerializer/NormalizerInterface';
import { Element } from '../XmlSerializer/Element';

export default class PointNormalizer implements NormalizerInterface<Point> {
    public fromJson(pointElement: Element): Point {
        return {
            x: parseFloat(pointElement.attributes.x),
            y: parseFloat(pointElement.attributes.y),
        };
    }
}
