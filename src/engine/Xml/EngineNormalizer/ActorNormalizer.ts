import Actor from 'engine/Scene/Actor/Actor';
import NormalizerCollection from '../XmlSerializer/NormalizerCollection';
import { Texture } from 'pixi.js';
import { Element } from '../XmlSerializer/Element';
import SpriteNormalizer from './SpriteNormalizer';

export default class ActorNormalizer extends SpriteNormalizer {
    public fromJson(actorElement: Element, normalizers: NormalizerCollection): Actor {
        const actor = new Actor(this.getElementTexture(actorElement, normalizers));

        this.initializeActor(actor, actorElement, normalizers);

        return actor;
    }

    protected initializeActor(actor: Actor, actorElement: Element, normalizers: NormalizerCollection): void {
        const {name, obstacle, radius} = actorElement.attributes;

        this.initializeSprite(actor, actorElement);

        if (radius) {
            actor.setRadius(parseInt(radius, 10));
        }

        let hasPolygon: boolean = false;

        actorElement.elements.forEach(element => {
            switch (element.name) {
                case 'Texture':
                    // Texture is loaded before creating the Sprite.
                    break;

                case 'Shape':
                    const shape = normalizers
                        .getNormalizer<Path>('Path')
                        .fromJson(element, normalizers)
                    ;

                    actor.setShape(shape);
                    break;

                case 'Polygon':
                    hasPolygon = true;
                    actor.setPolygon(normalizers.fromJson(element));

                    if (obstacle) {
                        actor.setObstacle('true' === obstacle);
                    }

                    break;

                default:
                    throw new Error(`Unexpected element <${element.name} /> in Actor element`);
            }
        });

        if ('true' === obstacle && !hasPolygon) {
            throw new Error(`Actor "${name}" is an obstacle but don't have polygon`);
        }
    }

    protected getElementTexture(actorElement: Element, normalizers: NormalizerCollection): Texture {
        const textureElement = actorElement.elements.find(element => 'Texture' === element.name);

        if (!textureElement) {
            return null;
        }

        return normalizers.fromJson(textureElement);
    }
}
