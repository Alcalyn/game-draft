import NormalizerInterface from '../XmlSerializer/NormalizerInterface';
import Room from 'engine/Scene/Room/Room';
import NormalizerCollection from '../XmlSerializer/NormalizerCollection';
import { Element } from '../XmlSerializer/Element';
import Polygons from 'engine/Geometry/Polygons';

export default class RoomNormalizer implements NormalizerInterface<Room> {
    public fromJson(json: Element, normalizers: NormalizerCollection): Room {
        const room = new Room();

        const areaElement = json.elements.find(element => 'Area' === element.name);
        const ring = normalizers
            .getNormalizer<Ring>('Ring')
            .fromJson(areaElement, normalizers)
        ;

        let polygon = [ring];

        if (areaElement.attributes && areaElement.attributes.scale) {
            polygon = Polygons.scale(polygon, parseFloat(areaElement.attributes.scale));
        }

        room.setArea(polygon);

        return room;
    }
}
