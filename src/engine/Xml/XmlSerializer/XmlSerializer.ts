import { xml2js, Options } from 'xml-js';
import FileLoaderInterface from './FileLoaderInterface';
import NormalizerInterface from './NormalizerInterface';
import NormalizerCollection from './NormalizerCollection';
import { Element } from './Element';
import PreprocessorInterface from './PreprocessorInterface';

export default class XmlSerializer {
    private normalizers: NormalizerCollection = new NormalizerCollection();

    private fileLoader: FileLoaderInterface;

    private preprocessors: PreprocessorInterface[] = [];

    public constructor(fileLoader: FileLoaderInterface) {
        this.fileLoader = fileLoader;
    }

    public importFile<T>(filename: string): Promise<T> {
        return this.fileLoader
            .loadFile(filename)
            .then(xml => this.import<T>(xml))
        ;
    }

    public import<T>(xml: string): T {
        const options: Options.XML2JS = {
            compact: false,
            ignoreDeclaration: true,
            ignoreInstruction: true,
            ignoreComment: true,
            ignoreDoctype: true,
        };

        const root: Element = xml2js(xml, options) as Element;

        this.preprocessors.forEach(preprocessor => preprocessor.preprocess(root));

        if (root.elements.length !== 1) {
            throw new Error(`The must have exactly one root in xml, found ${root.elements.length}`);
        }

        const {name} = root.elements[0];

        return this.normalizers
            .getNormalizer<T>(name)
            .fromJson(root.elements[0], this.normalizers)
        ;
    }

    public registerNormalizer<T>(tag: string, normalizer: NormalizerInterface<T>): this {
        this.normalizers.registerNormalizer<T>(tag, normalizer);

        return this;
    }

    public registerPreprocessor(preprocessor: PreprocessorInterface) {
        this.preprocessors.push(preprocessor);
    }
}
