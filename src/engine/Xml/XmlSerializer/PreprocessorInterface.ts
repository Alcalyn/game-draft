import { Element } from './Element';

export default interface PreprocessorInterface {
    /**
     * Modify element before using it.
     */
    preprocess(element: Element): void;
}
