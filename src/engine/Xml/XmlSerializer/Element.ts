/*
 * Same from the ones in 'xml-js' library,
 * but with modifications resulted from options
 * passed while xml parsing.
 */

export interface Attributes {
    [key: string]: string | undefined;
}

export interface Element {
    name?: string;
    type?: string;
    attributes?: Attributes;
    elements?: Element[];
    text?: string;
    cdata?: string;
}
