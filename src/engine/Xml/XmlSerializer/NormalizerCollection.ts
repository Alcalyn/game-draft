import NormalizerInterface from './NormalizerInterface';
import { Element } from './Element';

export default class NormalizerCollection {
    private normalizers: {[key: string]: NormalizerInterface<any>} = {};

    public fromJson<T>(element: Element): T {
        return this
            .getNormalizer<T>(element.name)
            .fromJson(element, this)
        ;
    }

    /**
     * @throws {Error} If there is no normalizer registered for this tag.
     */
    public getNormalizer<T>(tag: string): NormalizerInterface<T> {
        if (!(tag in this.normalizers)) {
            throw new Error(`There is no normalizer registered for tag "${tag}"`);
        }

        return this.normalizers[tag];
    }

    public registerNormalizer<T>(tag: string, normalizer: NormalizerInterface<T>): this {
        this.normalizers[tag] = normalizer;

        return this;
    }
}
