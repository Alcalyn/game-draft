import NormalizerCollection from './NormalizerCollection';
import { Element } from './Element';

export default interface NormalizerInterface<T> {
    fromJson(json: Element, normalizers: NormalizerCollection): T;
}
