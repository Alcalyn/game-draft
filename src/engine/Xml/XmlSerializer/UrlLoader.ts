import FileLoader from './FileLoaderInterface';

export default class UrlLoader implements FileLoader {
    public loadFile(filename: string): Promise<string> {
        return fetch(filename)
            .then(response => response.text())
        ;
    }
}
