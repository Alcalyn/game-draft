import PreprocessorInterface from './PreprocessorInterface';
import { Element } from './Element';

/**
 * Preprocessor to replace all element having a `ref` attribute
 * by the one having the `name` attribute with the same value.
 *
 * Used to reuse a same element, and override it.
 *
 * @example
 *
 * <Base name="base" attr="value">
 *   <Child>some value</Child>
 * </Base>
 * <Element ref="base" />
 *
 * Results in:
 *
 * <Base name="base" attr="value">
 *   <Child>some value</Child>
 * </Base>
 * <Base ref="base" attr="value">
 *   <Child>some value</Child>
 * </Base>
 */
export default class RefNameReplacePreprocessor implements PreprocessorInterface {
    private refs: {ref: string, refElement: Element}[];
    private names: {[key: string]: Element};

    public preprocess(element: Element): void {
        this.refs = [];
        this.names = {};

        this.findRefsAndNames(element);
        this.checkRefsAndNames();

        this.refs.forEach(({ref, refElement}) => {
            this.replaceRefElement(refElement, this.names[ref]);
        });
    }

    private replaceRefElement(ref: Element, base: Element): void {
        // Ref tag name is replaced by the base tag name
        ref.name = base.name;
        ref.type = base.type;

        // Base attributes are used as default (except "name")
        Object.entries(base.attributes).forEach(([attribute, value]) => {
            if ('name' === attribute) {
                return;
            }

            if (!ref.attributes[attribute]) {
                ref.attributes[attribute] = value;
            }
        });

        // Base elements are just appended as is in the ref element
        if (base.elements && !ref.elements) {
            ref.elements = [];
        }

        base.elements.forEach(element => {
            ref.elements.push(element);
        });
    }

    private findRefsAndNames(element: Element): void {
        let parseChildren: boolean = true;

        if (element.attributes) {
            const {ref, name} = element.attributes;

            if (ref) {
                this.refs.push({ref, refElement: element});
                parseChildren = false;
            }

            if (name) {
                if (this.names[name]) {
                    throw new Error(`Multiple elements having the same name "${name}"`);
                }

                this.names[name] = element;
                parseChildren = false;
            }
        }

        if (parseChildren && element.elements) {
            element.elements.forEach(childElement => this.findRefsAndNames(childElement));
        }
    }

    private checkRefsAndNames(): void {
        this.refs.forEach(({ref}) => {
            if (!this.names[ref]) {
                throw new Error(`Element is referencing ${ref}, but no element have this name`);
            }
        });
    }
}
