import fs from 'fs';
import FileLoader from './FileLoaderInterface';

export default class FileSystemLoader implements FileLoader {
    public loadFile(filename: string): Promise<string> {
        return new Promise((resolve, reject) => {
            fs.readFile(filename, (err, data) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(String(data));
            });
        });
    }
}
