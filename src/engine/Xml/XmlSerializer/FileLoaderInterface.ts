export default interface FileLoaderInterface {
    loadFile(filename: string): Promise<string>;
}
