import XmlSerializer from './XmlSerializer/XmlSerializer';
import FileLoaderInterface from './XmlSerializer/FileLoaderInterface';
import UrlLoader from './XmlSerializer/UrlLoader';
import SceneNormalizer from 'engine/Xml/EngineNormalizer/SceneNormalizer';
import ActorNormalizer from 'engine/Xml/EngineNormalizer/ActorNormalizer';
import SpriteNormalizer from 'engine/Xml/EngineNormalizer/SpriteNormalizer';
import TextureNormalizer from 'engine/Xml/EngineNormalizer/TextureNormalizer';
import PointNormalizer from 'engine/Xml/EngineNormalizer/PointNormalizer';
import PathNormalizer from 'engine/Xml/EngineNormalizer/PathNormalizer';
import RefNameReplacePreprocessor from './XmlSerializer/RefNameReplacePreprocessor';
import ActorSlotNormalizer from './EngineNormalizer/ActorSlotNormalizer';
import PolygonNormalizer from './EngineNormalizer/PolygonNormalizer';
import RoomNormalizer from './EngineNormalizer/RoomNormalizer';
import DoorNormalizer from './EngineNormalizer/DoorNormalizer';
import ItemNormalizer from './EngineNormalizer/ItemNormalizer';

export default class EngineSerializer extends XmlSerializer {
    constructor(fileLoader?: FileLoaderInterface) {
        super(fileLoader || new UrlLoader());

        this
            .registerNormalizer('Actor', new ActorNormalizer())
            .registerNormalizer('ActorSlot', new ActorSlotNormalizer())
            .registerNormalizer('Door', new DoorNormalizer())
            .registerNormalizer('Item', new ItemNormalizer())
            .registerNormalizer('Path', new PathNormalizer())
            .registerNormalizer('Point', new PointNormalizer())
            .registerNormalizer('Polygon', new PolygonNormalizer())
            .registerNormalizer('Ring', new PathNormalizer())
            .registerNormalizer('Room', new RoomNormalizer())
            .registerNormalizer('Scene', new SceneNormalizer())
            .registerNormalizer('Sprite', new SpriteNormalizer())
            .registerNormalizer('Texture', new TextureNormalizer())
        ;

        this
            .registerPreprocessor(new RefNameReplacePreprocessor())
        ;
    }
}
