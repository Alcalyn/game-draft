import 'engine/Geometry/Types';
import { Node, Graph, Dijkstra } from 'dijkstra-pathfinder';
import Points from 'engine/Geometry/Points';
import Polygons from 'engine/Geometry/Polygons';
import RestrictedArea from 'engine/Scene/Room/RestrictedArea';

export default class PathFinder {
    /**
     * Adapt Dijkstra graph to shortest path calculation
     * by adding start and end node and required arcs.
     */
    private static addStartAndEndNodesToGraph(graph: Graph, start: Point, end: Point, polygon: Polygon): void {
        // Adapt graph by adding path start and end
        const nodeStart = new Node(start);
        const nodeEnd = new Node(end);

        graph
            .addNode(nodeStart)
            .addNode(nodeEnd)
        ;

        [nodeStart, nodeEnd].forEach(newNode => {
            graph.nodes.forEach(graphNode => {
                if (graphNode === nodeStart || graphNode === nodeEnd) {
                    return;
                }

                if (!Polygons.segmentInsidePolygon(polygon, [newNode.payload, graphNode.payload])) {
                    return;
                }

                graph.addArc(
                    newNode,
                    graphNode,
                    Points.distance([newNode.payload, graphNode.payload]),
                );
            });
        });
    }

    public static pathFinderPolygon(start: Point, end: Point, polygon: Polygon): Path|null {
        // Returns true now if there is a direct path from start to end.
        if (Polygons.segmentInsidePolygon(polygon, [start, end])) {
            return [
                start,
                end,
            ];
        }

        const graph = PathFinder.createDijkstraGraphFromPolygon(polygon);

        PathFinder.addStartAndEndNodesToGraph(graph, start, end, polygon);

        const dijkstra = new Dijkstra(graph, graph.findNodeByPayload(start));

        dijkstra.calculate();

        const shortestPath = dijkstra.getPathTo(graph.findNodeByPayload(end));

        if (null === shortestPath) {
            return null;
        }

        return shortestPath
            .map(node => node.payload)
        ;
    }

    public static pathFinderRestrictedArea(start: Point, end: Point, restrictedArea: RestrictedArea): Path|null {
        start = Polygons.nearestPointInsidePolygon(start, restrictedArea.getInnerArea());

        return PathFinder.pathFinderPolygon(start, end, restrictedArea.getArea());
    }

    public static createDijkstraGraphFromPolygon(polygon: Polygon): Graph {
        const graph = new Graph();
        const polygonConcavePoints: Point[] = Polygons.getConcavePoints(polygon);
        const hashMapPolygonPoints: {[key: string]: [Point, Node]} = {};

        // Add all polygon point as graph node
        polygonConcavePoints.forEach(point => {
            const node = new Node(point);

            graph.addNode(node);

            hashMapPolygonPoints[`${point.x} ${point.y}`] = [point, node];
        });

        // Add all possible path between polygon points
        Object.values(hashMapPolygonPoints).forEach(pointNodeFrom => {
            Object.values(hashMapPolygonPoints).forEach(pointNodeTo => {
                if (pointNodeFrom === pointNodeTo) {
                    return;
                }

                const isInsidePolygon = Polygons.segmentInsidePolygon(
                    polygon,
                    [pointNodeFrom[0], pointNodeTo[0]],
                );

                if (!isInsidePolygon) {
                    return;
                }

                graph.addOrientedArc(
                    pointNodeFrom[1],
                    pointNodeTo[1],
                    Points.distance([
                        pointNodeFrom[0],
                        pointNodeTo[0],
                    ]),
                );
            });
        });

        return graph;
    }
}
