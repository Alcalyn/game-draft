import { Application, InteractionEvent } from 'pixi.js';
import { Viewport } from 'pixi-viewport';
import Scene from 'engine/Scene/Scene';

/**
 * Base class for a game.
 *
 * Displays a Scene, should be added to html with `document.body.appendChild(game.view)`
 */
export default class Game extends Application {
    public readonly viewport: Viewport;

    /**
     * Scene currently displayed.
     */
    private scene: Scene = null;

    constructor() {
        super({
            width: window.innerWidth,
            height: window.innerHeight,
            backgroundColor: 0xffffff,
            autoStart: true,
            sharedTicker: true,
            sharedLoader: true,
            antialias: true,
        });

        this.viewport = new Viewport({
            screenWidth: window.innerWidth,
            screenHeight: window.innerHeight,
        });

        this.stage.addChild(this.viewport);
    }

    public getScene(): Scene {
        return this.scene;
    }

    public loadScene(scene: Scene): Game {
        if (null !== this.scene) {
            this.viewport.removeChild(this.scene);
        }

        this.scene = scene;
        this.viewport.addChild(scene);

        return this;
    }

    /**
     * Return point coordinates translated to viewport
     * from event fired on screen.
     */
    public getPointFromEvent(event: InteractionEvent): Point {
        return this.viewport.toWorld(event.data.global);
    }

    public onClick(callback: (point: Point) => void): Game {
        const clickedToViewport = (event: InteractionEvent) => {
            callback(this.getPointFromEvent(event));
        };

        this.viewport.on('touchstart', clickedToViewport, this);
        this.viewport.on('touchmove', clickedToViewport, this);
        this.viewport.on('mousedown', clickedToViewport, this);

        // Trigger event on drag
        let drag: boolean = false;
        this.viewport.on('mousedown', () => drag = true, this);
        this.viewport.on('mouseup', () => drag = false, this);
        this.viewport.on('mousemove', event => drag && clickedToViewport(event), this);

        return this;
    }
}
