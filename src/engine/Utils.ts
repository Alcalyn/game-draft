export default class Utils {
    public static chunkPair<T>(array: T[]): [T, T][] {
        const chunked: [T, T][] = [];

        for (let i = 0; i < array.length; i += 2) {
            chunked.push([
                array[i],
                array[i + 1],
            ]);
        }

        return chunked;
    }
}
