import Actor from 'engine/Scene/Actor/Actor';
import Inventory from './Inventory';

/**
 * Object that can be looted and placed into an inventory.
 */
export default class Item extends Actor {
    /**
     * The inventory where this item is placed.
     */
    private inventory: Inventory = null;

    public hasInventory(): boolean {
        return null === this.inventory;
    }

    public getInventory(): Inventory {
        return this.inventory;
    }

    public setInventory(inventory: Inventory): this {
        this.inventory = inventory;

        this.visible = null === inventory;

        return this;
    }
}
