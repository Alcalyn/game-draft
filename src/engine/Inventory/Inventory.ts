import Item from './Item';

export default class Inventory extends Set<Item> {
    public add(item: Item): this {
        super.add(item);

        item.setInventory(this);

        return this;
    }
}
