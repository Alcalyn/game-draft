import { EventEmitter } from 'events';
import Action from 'engine/Action/Action';

/**
 * Handle a list of actions to perform sequencially.
 * Useful i.e for characters to follow a path, then do another action...
 *
 * @fires ActionStack#action.stacked
 * @fires ActionStack#action.started
 * @fires ActionStack#action.finished
 * @fires ActionStack#stack.empty
 */
export default class ActionStack<T extends Action> extends EventEmitter {
    /**
     * When a new event has been stacked.
     *
     * @event ActionStack#action.stacked
     * @param {Action} action The action that just have been started.
     */
    public static readonly ACTION_STACKED = 'action.stacked';

    /**
     * When action just started.
     *
     * @event ActionStack#action.started
     * @param {Action} action The action that just have been started.
     */
    public static readonly ACTION_STARTED = 'action.started';

    /**
     * When action is well finished.
     * Not trigered when action is aborted.
     *
     * @event ActionStack#action.finished
     * @param {Action} action The action that just have been executed.
     */
    public static readonly ACTION_FINISHED = 'action.finished';

    /**
     * When current action is done and there is no other stacked action.
     *
     * @event ActionStack#stack.empty
     */
    public static readonly STACK_EMPTY = 'stack.empty';

    /**
     * The action currently being executed.
     */
    private currentAction: T = null;

    /**
     * Next actions to execute once currentAction is done.
     */
    private stackedActions: T[] = [];

    /**
     * Whether this ActionStack is executing actions
     */
    private started: boolean = false;

    public constructor() {
        super();

        this.start();

        this.on(ActionStack.ACTION_STACKED, () => {
            if (this.hasCurrentAction()) {
                return;
            }

            this.executeNextAction();
        });
    }

    public hasCurrentAction(): boolean {
        return null !== this.currentAction;
    }

    public getCurrentAction(): T {
        return this.currentAction;
    }

    public hasStackedActions(): boolean {
        return this.stackedActions.length > 0;
    }

    public getStackedActions(): T[] {
        return this.stackedActions;
    }

    public getNextStackedAction(): T {
        if (!this.hasStackedActions()) {
            return null;
        }

        return this.stackedActions[0];
    }

    public stackAction(action: T): void {
        this.stackedActions.push(action);
        this.emit(ActionStack.ACTION_STACKED, action);

        if (this.started && !this.hasCurrentAction()) {
            this.executeNextAction();
        }
    }

    public clearActions(): void {
        if (this.hasCurrentAction()) {
            this.currentAction.emit(Action.ACTION_ABORTED);
        }

        this.currentAction = null;
        this.stackedActions = [];
    }

    public isStarted(): boolean {
        return this.started;
    }

    /**
     * Executes next actions sequencially until `stop()` is called.
     */
    public start(): void {
        if (this.started) {
            return;
        }

        this.started = true;

        if (!this.hasCurrentAction()) {
            this.executeNextAction();
        }
    }

    /**
     * Stops next actions executions.
     * Current action is still executed until it finishes.
     */
    public stop(): void {
        this.started = false;
    }

    private executeNextAction(): void {
        if (!this.started) {
            return;
        }

        const action = this.nextAction();

        if (null === action) {
            return;
        }

        action.execute(() => {
            this.currentAction = null;

            action.emit(Action.ACTION_FINISHED);
            this.emit(ActionStack.ACTION_FINISHED, action);

            if (!this.hasStackedActions()) {
                this.emit(ActionStack.STACK_EMPTY);
                return;
            }

            this.executeNextAction();
        });

        action.emit(Action.ACTION_STARTED);
        this.emit(ActionStack.ACTION_STARTED, action);
    }

    /**
     * Unstack next action and return it.
     * Returns null if it no other stacked action.
     */
    private nextAction(): T {
        if (!this.hasStackedActions()) {
            this.currentAction = null;

            return null;
        }

        return this.currentAction = this.stackedActions.shift();
    }
}
