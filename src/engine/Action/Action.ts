import { EventEmitter } from 'events';

/**
 * @fires Action#action.started
 * @fires Action#action.finished
 * @fires Action#action.aborted
 */
export default abstract class Action extends EventEmitter {
    /**
     * When action just started.
     *
     * @event Action#action.started
     */
    public static readonly ACTION_STARTED = 'action.started';

    /**
     * When action is well finished.
     * Not trigered when action is aborted.
     *
     * @event Action#action.finished
     */
    public static readonly ACTION_FINISHED = 'action.finished';

    /**
     * When ActionStack is cleared and current action is aborted.
     *
     * @event Action#action.aborted
     */
    public static readonly ACTION_ABORTED = 'action.aborted';

    /**
     * Singleton for asPromise().
     */
    private promise: Promise<void> = null;

    /**
     * Promise that can be used to use then() or finally() on this action.
     * Beware not to forget to catch promise for aborted actions.
     */
    public asPromise(): Promise<void> {
        if (null === this.promise) {
            this.promise = new Promise((resolve, reject) => {
                this.once(Action.ACTION_FINISHED, resolve);
                this.once(Action.ACTION_ABORTED, () => reject('Action aborted.'));
            });
        }

        return this.promise;
    }

    public abstract execute(done: () => void): void;
}
