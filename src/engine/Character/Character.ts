import Actor from 'engine/Scene/Actor/Actor';
import RestrictedArea from '../Scene/Room/RestrictedArea';
import ActionStack from 'engine/Action/ActionStack';
import MoveAction from 'engine/Character/Action/MoveAction';
import TravelAction from 'engine/Character/Action/TravelAction';
import AbstractCharacterAction from './Action/AbstractCharactedAction';
import Action from 'engine/Action/Action';
import { Ticker } from 'pixi.js';
import HitTest from 'engine/HitTest';

/**
 * Base class for Character.
 *
 * A character can move on the scene,
 * avoid obstacle, can be behind other actors...
 *
 * @fires Character#move.start
 * @fires Character#move.stop
 * @fires Character#move.new_target
 * @fires Character#hittest.enters_actor
 * @fires Character#hittest.leaves_actor
 */
export default class Character extends Actor {
    /**
     * When character is now moving,
     * i.e when moveAt() has been called.
     *
     * @event Character#move.start
     */
    public static readonly MOVE_START = 'move.start';

    /**
     * When character is no longer moving
     * i.e when character reached his target.
     *
     * @event Character#move.stop
     */
    public static readonly MOVE_STOP = 'move.stop';

    /**
     * When character is moving to a new target.
     * Fired i.e during a travel, when character
     * reached a step, and is now going to a new step.
     *
     * Can be used to change character rotation.
     *
     * @event Character#move.new_target
     * @param {Point} target Coordinates to the new target.
     */
    public static readonly MOVE_NEW_TARGET = 'move.new_target';

    /**
     * When character is now hitting another actor,
     * or inside a polygon of another actor.
     *
     * Use method `listenHitTestWith()` to enable this event.
     *
     * @event Character#hittest.enters_actor
     * @param {Actor} actor Actor that this character is now hitting.
     */
    public static readonly HITTEST_ENTERS_ACTOR = 'hittest.enters_actor';

    /**
     * When character is no longer hitting another actor,
     * or now outside a polygon of another actor.
     *
     * Use method `listenHitTestWith()` to enable this event.
     *
     * @event Character#hittest.leaves_actor
     * @param {Actor} actor Actor that this character is no longer hitting.
     */
    public static readonly HITTEST_LEAVES_ACTOR = 'hittest.leaves_actor';

    /**
     * If set, character won't be able to move outside this area.
     */
    protected restrictedArea: RestrictedArea = null;

    /**
     * The list of actions the character have to execute sequencially.
     * I.e following a path requires to move to a point, then another...
     */
    protected actionStack = new ActionStack<AbstractCharacterAction>();

    /**
     * The moving speed of the character.
     * In pixels per image, for 60 images per seconds.
     */
    protected moveSpeed: number = 5;

    public constructor(texture?: PIXI.Texture) {
        super(texture);
    }

    public getRestrictedArea(): RestrictedArea {
        return this.restrictedArea;
    }

    public setRestrictedArea(restrictedArea: RestrictedArea): Character {
        this.restrictedArea = restrictedArea;

        return this;
    }

    public isMoving(): boolean {
        return this.actionStack.getCurrentAction() instanceof MoveAction;
    }

    public getMoveSpeed(): number {
        return this.moveSpeed;
    }

    public setMoveSpeed(moveSpeed: number): Character {
        this.moveSpeed = moveSpeed;

        return this;
    }

    public stackAction(characterAction: AbstractCharacterAction): this {
        this.actionStack.stackAction(characterAction);

        return this;
    }

    public moveTo(target: Point, stack: boolean = false): TravelAction {
        const wasMoving = this.isMoving();

        if (!stack) {
            this.actionStack.clearActions();
        }

        const action = new TravelAction(this, target);

        action
            .on(Character.MOVE_NEW_TARGET, (moveTarget: Point) => {
                this.emit(Character.MOVE_NEW_TARGET, moveTarget);
            })
            .once(Action.ACTION_FINISHED, () => {
                if (!(this.actionStack.getNextStackedAction() instanceof MoveAction)) {
                    this.emit(Character.MOVE_STOP);
                }
            })
        ;

        this.actionStack.stackAction(action);

        if (!wasMoving) {
            this.emit(Character.MOVE_START);
        }

        return action;
    }

    public stopActions(): this {
        const wasMoving = this.isMoving();

        this.actionStack.clearActions();

        if (wasMoving) {
            this.emit(Character.MOVE_STOP);
        }

        return this;
    }

    public listenHitTestWith(actor: Actor): this {
        let wasInside = HitTest.actorIsInside(this, actor);

        Ticker.shared.add(() => {
            const nowInside = HitTest.actorIsInside(this, actor);

            if (!wasInside && nowInside) {
                this.emit(Character.HITTEST_ENTERS_ACTOR, actor);
            }

            if (wasInside && !nowInside) {
                this.emit(Character.HITTEST_LEAVES_ACTOR, actor);
            }

            wasInside = nowInside;
        });

        return this;
    }
}
