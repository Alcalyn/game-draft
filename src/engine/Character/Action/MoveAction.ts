import 'engine/Geometry/Types';
import { Ticker } from 'pixi.js';
import AbstractCharacterAction from './AbstractCharactedAction';
import Character from '../Character';
import Points from 'engine/Geometry/Points';
import ActorContainer from 'engine/Scene/Actor/ActorContainer';
import Action from 'engine/Action/Action';

/**
 * A SimpleMoveAction to make the character move to a point
 * following a straight line.
 *
 * @see TravelAction To tell the character follow the pathfinder.
 */
export default class MoveAction extends AbstractCharacterAction {
    /**
     * Where the character should move to.
     */
    protected moveTarget: Point;

    public constructor(character: Character, moveTarget: Point) {
        super(character);

        this.moveTarget = moveTarget;
    }

    public getMoveTarget(): Point {
        return this.moveTarget;
    }

    public execute(done: () => void): void {
        const moveUpdate = (delta: number) => {
            const step = this.character.getMoveSpeed() * delta;
            const direction = Math.atan2(
                this.moveTarget.y - this.character.y,
                this.moveTarget.x - this.character.x,
            );

            if (Points.distance([this.character, this.moveTarget]) < step) {
                this.character.position.set(this.moveTarget.x, this.moveTarget.y);
                done();
                return;
            }

            this.character.x += Math.cos(direction) * step;
            this.character.y += Math.sin(direction) * step;

            if (this.character.parent instanceof ActorContainer) {
                this.character.parent.sortDirty = true;
            }
        };

        this.asPromise().catch(() => void 0).finally(() => {
            Ticker.shared.remove(moveUpdate);
        });

        Ticker.shared.add(moveUpdate);
    }
}
