import 'engine/Geometry/Types';
import Character from '../Character';
import ActionStack from 'engine/Action/ActionStack';
import MoveAction from './MoveAction';
import Polygons from 'engine/Geometry/Polygons';
import PathFinder from 'engine/PathFinder';
import Action from 'engine/Action/Action';

/**
 * A move action to tell the character to travel to a point,
 * avoiding obstacle and following pathfinder result.
 *
 * @fires Character.MOVE_NEW_TARGET When character reached a travel step and has a new move target
 */
export default class TravelAction extends MoveAction {
    /**
     * The path to follow to go to current move target.
     * Used to save path from pathfinder result.
     */
    private movePath = new ActionStack<MoveAction>();

    public constructor(character: Character, moveTarget: Point) {
        super(character, moveTarget);
    }

    public execute(done: () => void): void {
        const restrictedArea = this.character.getRestrictedArea();
        const target = Polygons.nearestPointInsidePolygon(this.moveTarget, restrictedArea.getInnerArea());
        const bestPath = PathFinder.pathFinderRestrictedArea(this.character.position, target, restrictedArea);

        if (null === bestPath) {
            throw new Error('No path found');
        }

        this.movePath
            .on(ActionStack.STACK_EMPTY, done)
            .on(ActionStack.ACTION_STARTED, (action: MoveAction) => {
                this.emit(Character.MOVE_NEW_TARGET, action.getMoveTarget());
            })
        ;

        this.once(Action.ACTION_ABORTED, () => {
            this.movePath.clearActions();
        });

        bestPath.slice(1).forEach(point => {
            this.movePath.stackAction(new MoveAction(this.character, point));
        });
    }
}
