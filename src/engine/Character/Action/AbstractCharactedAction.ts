import 'engine/Geometry/Types';
import Action from 'engine/Action/Action';
import Character from '../Character';

export default abstract class AbstractCharacterAction extends Action {
    protected character: Character;

    public constructor(character: Character) {
        super();

        this.character = character;
    }
}
