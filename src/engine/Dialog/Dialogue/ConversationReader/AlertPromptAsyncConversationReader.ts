import AbstractConversationReader from '../AbstractConversationReader';
import ConversationNode from '../ConversationNode';

/**
 * Play a conversation using alert() and prompt() methods,
 * and time delay between nodes.
 * Can be used for conversation testing purposes, or as an example
 * of an asyncronous conversation.
 */
export default class AlertPromptAsyncConversationReader extends AbstractConversationReader<ConversationNode> {
    protected displayText(node: ConversationNode, done: () => void): void {
        setTimeout(() => {
            alert(node.text);
            done();
        }, 500);
    }

    protected displayChoices(nodes: ConversationNode[], choose: (node: ConversationNode) => void): void {
        const promptText = nodes
            .map((node, index) => `${index}> ${node.title || node.text}`)
            .join('\n')
        ;

        let choosenNode: ConversationNode;

        do {
            choosenNode = nodes[parseInt(prompt(promptText), 10)];
        } while (!nodes.includes(choosenNode));

        setTimeout(() => {
            choose(choosenNode);
        }, 500);
    }
}
