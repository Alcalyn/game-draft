import AbstractConversationReader from '../AbstractConversationReader';
import ConversationNode from '../ConversationNode';

/**
 * Play a conversation using alert() and prompt() methods.
 * Can be used for conversation testing purposes, or as an example
 * of a syncronous conversation.
 */
export default class AlertPromptConversationReader extends AbstractConversationReader<ConversationNode> {
    protected displayText(node: ConversationNode, done: () => void): void {
        alert(node.text);
        done();
    }

    protected displayChoices(nodes: ConversationNode[], choose: (node: ConversationNode) => void): void {
        const promptText = nodes
            .map((node, index) => `${index}> ${node.title || node.text}`)
            .join('\n')
        ;

        let choosenNode: ConversationNode;

        do {
            choosenNode = nodes[parseInt(prompt(promptText), 10)];
        } while (!nodes.includes(choosenNode));

        choose(choosenNode);
    }
}
