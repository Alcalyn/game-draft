import ConversationNode from './ConversationNode';

/**
 * A conversation reader.
 *
 * Extends it to implement the way to display dialogs
 * and prompt choices.
 *
 * Generic T is the type of nodes this conversation handles.
 */
export default abstract class AbstractConversationReader<T extends ConversationNode> {

    /**
     * Starts conversation from this node.
     */
    public async play(node: T): Promise<void> {
        if (node.action) {
            await this.actionPromise(node.action);
        }

        if (node.text) {
            await this.displayTextPromise(node);
        }

        await this.playNext(node);
    }

    /**
     * Recursive method to play next node depending on current node.
     */
    private async playNext(node: T): Promise<void> {
        const nodeChildren = !Array.isArray(node.children)
            ? []
            : node.children.filter(childNode => {
                if (!childNode.condition) {
                    return true;
                }

                return childNode.condition(node, childNode);
            })
        ;

        if (0 === nodeChildren.length) {
            return;
        }

        const isChoice = node.isChoice === undefined
            ? nodeChildren.length > 1
            : node.isChoice
        ;

        if (!isChoice) {
            return await this.play(nodeChildren[0]);
        }

        const choosenNode = await this.displayChoicesPromise(nodeChildren);

        if (!nodeChildren.includes(choosenNode)) {
            throw new Error('choose() called on a node that is not a children');
        }

        await this.play(choosenNode);
    }

    /**
     * Display node text,
     * then done() callback must be called to continue conversation.
     */
    protected abstract displayText(node: T, done: () => void): void;

    /**
     * Prompt choices from nodes,
     * then choose(node) must be called to continue conversation with the choosen node.
     */
    protected abstract displayChoices(nodes: T[], choose: (node: T) => void): void;

    private actionPromise(action: (done: () => void) => void): Promise<void> {
        return new Promise(resolve => {
            action(resolve);
        });
    }

    private displayTextPromise(node: T): Promise<void> {
        return new Promise(resolve => {
            this.displayText(node, resolve);
        });
    }

    private displayChoicesPromise(nodes: T[]): Promise<T> {
        return new Promise(resolve => {
            this.displayChoices(nodes, resolve);
        });
    }
}
