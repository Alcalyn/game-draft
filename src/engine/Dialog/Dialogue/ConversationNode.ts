/**
 * Conversation node.
 *
 * Defines which text is said by actor.
 *
 * Can be (one or many):
 *  - a simple text
 *  - a choice prompt if there is multiple children
 *  - a custom script (see action)
 */
export default interface ConversationNode {
    /**
     * The actual text the actor will say.
     * If empty (possible case i.e for choice prompt), nothing will be said.
     */
    text?: string;

    /**
     * An optional title that can be displayed in choices prompt.
     * If not set, node text will be displayed instead.
     */
    title?: string;

    /**
     * Whether this node must be a choice node or not.
     * If not set, choices will be displayed if there is multiple children.
     *
     * Useful to force a single children node to display as a choice with isChoice: true,
     * or to create a fallback branching model with isChoice: false and children with conditions.
     */
    isChoice?: boolean;

    /**
     * Next texts.
     *
     * If empty, the conversation ends.
     * If one child, it will be played linearly (except choice = true).
     * If multiple, choices will be prompted (except choice = false, first node will be played).
     */
    children?: this[];

    /**
     * Whether this node should be a visible children or not.
     */
    condition?: (parentNode: this, choiceNode: this) => boolean;

    /**
     * If set, this function will be called before all.
     * You must call done() in your function
     * to tell action is finished and continue conversation.
     *
     * To execute action after text instead of before,
     * just place action in a new node after text node.
     */
    action?: (done: () => void) => void;
}
