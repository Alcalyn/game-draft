import ConversationNode from './ConversationNode';

export default class ConversationFactory {
    /**
     * Chain all nodes to create a linear dialogue.
     */
    public static linear<T extends ConversationNode>(nodes: T[]): T {
        for (let i = 1; i < nodes.length; i++) {
            nodes[i - 1].children = [
                nodes[i],
            ];
        }

        return nodes[0];
    }
}
