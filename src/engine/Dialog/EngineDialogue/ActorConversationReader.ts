import AbstractConversationReader from '../Dialogue/AbstractConversationReader';
import ActorConversationNode from './ActorConversationNode';
import Dialog from './Dialog';

export default class ActorConversationReader extends AbstractConversationReader<ActorConversationNode> {
    protected displayText(node: ActorConversationNode, done: () => void): void {
        Dialog.actorSay(node.from, node.text);

        setTimeout(() => {
            Dialog.actorClean(node.from);
            done();
        }, 2000);
    }

    protected displayChoices(nodes: ActorConversationNode[], choose: (node: ActorConversationNode) => void): void {
        const promptText = nodes
            .map((node, index) => `${index}> ${node.title || node.text}`)
            .join('\n')
        ;

        let choosenNode: ActorConversationNode;

        do {
            choosenNode = nodes[parseInt(prompt(promptText), 10)];
        } while (!nodes.includes(choosenNode));

        choose(choosenNode);
    }
}
