import Actor from 'engine/Scene/Actor/Actor';
import ActorConversationNode from './ActorConversationNode';
import AbstractConversationReader from '../Dialogue/AbstractConversationReader';
import ConversationNode from '../Dialogue/ConversationNode';

export default class ConversationContainer {
    private conversations = new Map<Actor, Map<Actor, ActorConversationNode>>();

    private hasRunningConversation: boolean;

    public constructor(
        private reader: AbstractConversationReader<ConversationNode>,
    ) {}

    public async play(from: Actor, to: Actor): Promise<void> {
        if (this.hasRunningConversation) {
            return;
        }

        const conversation = this.get(from, to);

        if (undefined === conversation) {
            return;
        }

        this.hasRunningConversation = true;

        await this.reader.play(conversation);

        this.hasRunningConversation = false;
    }

    public set(from: Actor, to: Actor, rootNode: ActorConversationNode): void {
        let conversationsFrom = this.conversations.get(from);

        if (undefined === conversationsFrom) {
            conversationsFrom = new Map<Actor, ActorConversationNode>();
            this.conversations.set(from, conversationsFrom);
        }

        conversationsFrom.set(to, rootNode);
    }

    public get(from: Actor, to: Actor): ActorConversationNode {
        const conversationsFrom = this.conversations.get(from);

        if (undefined === conversationsFrom) {
            return;
        }

        return conversationsFrom.get(to);
    }
}
