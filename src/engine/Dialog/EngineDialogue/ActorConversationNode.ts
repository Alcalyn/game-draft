import ConversationNode from '../Dialogue/ConversationNode';
import Actor from 'engine/Scene/Actor/Actor';

export default interface ActorConversationNode extends ConversationNode {
    /**
     * Which actor is talking.
     * Used to display text next to actor on the scene.
     * If not set, text will be displayed on screen center bottom.
     */
    from?: Actor;
}
