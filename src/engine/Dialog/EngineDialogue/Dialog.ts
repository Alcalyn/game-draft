import { Text, TextStyle } from 'pixi.js';
import Actor from 'engine/Scene/Actor/Actor';

export default class Dialog {
    /**
     * All currently displayed text for each actors.
     * Makes sure actors say only one thing at a time.
     */
    private static actorsDialogs = new Map<Actor, {text: Text, timeout: any}>();

    public static actorSay(actor: Actor, say: string, style?: TextStyle): void {
        Dialog.actorClean(actor);

        const scene = actor.getScene();

        if (null === scene) {
            throw new Error('Actor must be on a scene to display what he is saying');
        }

        const text = new Text(say, style);

        text.position.x = actor.x;
        text.position.y = actor.y;

        text.roundPixels = true;
        text.anchor.set(0.5);

        scene.foreground.addChild(text);

        const timeout = setTimeout(() => Dialog.actorClean(actor), 3000);

        Dialog.actorsDialogs.set(actor, {text, timeout});
    }

    public static actorClean(actor: Actor): void {
        const actorDialog = Dialog.actorsDialogs.get(actor);

        if (actorDialog) {
            const { text, timeout } = actorDialog;

            text.parent.removeChild(text);
            Dialog.actorsDialogs.delete(actor);

            if (timeout) {
                clearTimeout(timeout);
            }
        }
    }
}
