import 'engine/Geometry/Types';
import MultiArmature from 'game/Character/MultiArmature';
import Character from 'engine/Character/Character';

/**
 * Extends Character to display a character animated with DragonBones.
 */
export default class DragonBonesCharacter extends MultiArmature {
    public static FRONT: number = 0;
    public static SIDE: number = 1;
    public static BACK: number = 2;

    private talkingThread: number;
    private blinkingThread: number;

    protected walkSpeed: number = 5;
    protected sizeScale: number = 0.1;

    public constructor(name: string, armatureResources: string[]) {
        super(armatureResources);

        this.name = name;

        this.on('armature_loaded', () => {
            this.scale.x = this.sizeScale;
            this.scale.y = this.sizeScale;

            this.startBlinking();
            this.play('idle');

            this.on(Character.MOVE_START, () => {
                this.startWalking();
            });
            this.on(Character.MOVE_STOP, () => {
                this.stopWalking();
            });
            this.on(Character.MOVE_NEW_TARGET, (target: Point) => {
                this.updateDirection(target);
            });
        });
    }

    public startWalking(): void {
        this.play('walk');
        this.startTalking();
    }

    public stopWalking(): void {
        this.play('idle');
        this.stopTalking();
    }

    public startTalking(): void {
        if (this.talkingThread) {
            return;
        }

        this.talkingThread = window.setInterval(() => {
            this.getAllSlots('mouth').forEach(slot => {
                slot.displayIndex = Math.floor(Math.random() * slot.displayFrameCount);
            });
        }, 100);
    }

    public stopTalking(): void {
        if (!this.talkingThread) {
            return;
        }

        clearInterval(this.talkingThread);
        this.getAllSlots('mouth').forEach(slot => slot.displayIndex = 4);
    }

    public startBlinking(): void {
        if (this.blinkingThread) {
            return;
        }

        this.blinkingThread = window.setTimeout(() => {
            this.closeEyes();
            setTimeout(() => {
                this.stopBlinking();
                this.startBlinking();
            }, 100);
        }, Math.random() * 3000);
    }

    public stopBlinking(): void {
        if (!this.blinkingThread) {
            return;
        }

        this.openEyes();
        this.blinkingThread = undefined;
    }

    public openEyes(): void {
        this.getAllSlots('eyes').forEach(slot => slot.displayIndex = 0);
    }

    public closeEyes(): void {
        this.getAllSlots('eyes').forEach(slot => slot.displayIndex = 1);
    }

    public updateDirection(target: Point): void {
        // Display character to left or right depending on current move target
        this.scale.x = this.sizeScale;

        if (Math.abs(target.x - this.x) > Math.abs(target.y - this.y)) {
            this.displayArmature(DragonBonesCharacter.SIDE);

            if (target.x < this.x) {
                this.scale.x = -this.sizeScale;
            }
        } else {
            this.displayArmature(target.y > this.y ? DragonBonesCharacter.FRONT : DragonBonesCharacter.BACK);
        }
    }
}
