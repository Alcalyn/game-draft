import DragonBonesCharacter from 'game/Character/DragonBonesCharacter';

export default class Girl extends DragonBonesCharacter {
    constructor() {
        super('girl', [
            'assets/dragonbones/girl/Front',
            'assets/dragonbones/girl/Front',
            'assets/dragonbones/girl/Front',
        ]);

        this.setRadius(15);
    }
}
