import { Loader, LoaderResource } from 'pixi.js';
import Character from '../../engine/Character/Character';

export default class MultiArmature extends Character {
    private armatures: dragonBones.PixiArmatureDisplay[];
    private currentIndex: number;

    public constructor(armatureResources: string[]) {
        super();

        armatureResources.forEach(armatureResource => {
            [
                armatureResource + '_ske.json',
                armatureResource + '_tex.json',
                armatureResource + '_tex.png',
            ]
                .filter(url => !Loader.shared.resources[url])
                .forEach(url => Loader.shared.add(url))
            ;
        });

        Loader.shared.onComplete.once((
            // @ts-ignore: '*' is declared but its value is never read.
            loader: Loader,
            resources: dragonBones.Map<LoaderResource>,
        ) => {
            this.armatures = armatureResources.map(armatureResource => {
                const factory = new dragonBones.PixiFactory();

                factory.parseDragonBonesData(resources[armatureResource + '_ske.json'].data);
                factory.parseTextureAtlasData(
                    resources[armatureResource + '_tex.json'].data,
                    resources[armatureResource + '_tex.png'].texture,
                );

                const armature = factory.buildArmatureDisplay('Armature');

                this.addChild(armature);

                return armature;
            });

            this.displayArmature(0);
            this.setTimeScale(5);

            this.emit('armature_loaded');
        });
    }

    public getArmatures(): dragonBones.PixiArmatureDisplay[] {
        return this.armatures;
    }

    public getCurrentIndex(): number {
        return this.currentIndex;
    }

    public getCurrentArmature(): dragonBones.PixiArmatureDisplay {
        return this.armatures[this.currentIndex];
    }

    public displayArmature(index: number): void {
        if (index === this.currentIndex) {
            return;
        }

        this.armatures.forEach(armature => {
            armature.visible = false;
        });

        this.armatures[index].visible = true;
        this.currentIndex = index;
    }

    public play(animation: string): void {
        this.armatures.forEach(armature => {
            armature.animation.play(animation);
        });
    }

    public setTimeScale(timeScale: number): void {
        this.armatures.forEach(armature => {
            armature.animation.timeScale = timeScale;
        });
    }

    public getAllSlots(name: string): dragonBones.Slot[] {
        return this.armatures
            .map(armature => armature.armature.getSlot(name))
            .filter(slot => slot)
        ;
    }
}
