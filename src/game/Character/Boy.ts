import DragonBonesCharacter from 'game/Character/DragonBonesCharacter';

export default class Boy extends DragonBonesCharacter {
    constructor() {
        super('boy', [
            'assets/dragonbones/boy/Front',
            'assets/dragonbones/boy/Side',
            'assets/dragonbones/boy/Back',
        ]);

        this.setRadius(15);
    }
}
