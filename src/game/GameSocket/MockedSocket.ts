import AbstractGameSocket from 'game/GameSocket/AbstractGameSocket';

export default class MockedSocket extends AbstractGameSocket {
    // @ts-ignore: '*' is declared but its value is never read.
    public send(type: string, data: any): void {
        // noop
    }
}
