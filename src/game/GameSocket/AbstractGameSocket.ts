import { utils } from 'pixi.js';

export default abstract class AbstractGameSocket extends utils.EventEmitter {
    private static readonly TEMPORIZE_TIME: number = 250;

    private temporizations: {[key: string]: {
        timeout: number,
        lastMessage: {type: string, data: any},
    }} = {};

    public abstract send(type: string, data: any): void;

    public sendTemporized(temporizeType: string, type: string, data: any): void {
        let temporization = this.temporizations[temporizeType];

        if (!temporization || !temporization.timeout) {
            temporization = {
                timeout: window.setTimeout(() => {
                    if (this.temporizations[temporizeType].lastMessage) {
                        const message = this.temporizations[temporizeType].lastMessage;
                        this.send(message.type, message.data);
                    }

                    this.temporizations[temporizeType] = {
                        timeout: undefined,
                        lastMessage: undefined,
                    };
                }, AbstractGameSocket.TEMPORIZE_TIME),
                lastMessage: undefined,
            };

            this.send(type, data);
        } else {
            temporization.lastMessage = {type, data};
        }

        this.temporizations[temporizeType] = temporization;
    }
}
