import AbstractGameSocket from 'game/GameSocket/AbstractGameSocket';

export default class Websocket extends AbstractGameSocket {
    private session: ab.Session;

    constructor(session: ab.Session) {
        super();

        this.session = session;

        console.log('Subscribing to app/game topic.');

        // @ts-ignore: '*' is declared but its value is never read.
        session.subscribe('app/game', (topic: string, message: {payload: string}) => {
            const data = JSON.parse(message.payload);
            this.emit(data._type, data);
        });
    }

    public send(type: string, data: any): void {
        data._type = type;
        this.session.publish('app/game', JSON.stringify(data), true);
    }
}
