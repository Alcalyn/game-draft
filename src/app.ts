/* tslint:disable:max-classes-per-file */
import { Point as PixiPoint, InteractionEvent } from 'pixi.js';
import Game from 'engine/Game';
import AbstractGameSocket from 'game/GameSocket/AbstractGameSocket';
import Websocket from 'game/GameSocket/Websocket';
import MockedSocket from 'game/GameSocket/MockedSocket';
import Girl from 'game/Character/Girl';
import Boy from 'game/Character/Boy';
import DragonBonesCharacter from 'game/Character/DragonBonesCharacter';
import Scene from 'engine/Scene/Scene';
import Polygons from 'engine/Geometry/Polygons';
import EngineSerializer from 'engine/Xml/EngineSerializer';
import Door from 'engine/Scene/Actor/Door';
import Character from 'engine/Character/Character';
import Actor from 'engine/Scene/Actor/Actor';
import Switch from 'engine/Switch';
import AbstractCharacterAction from 'engine/Character/Action/AbstractCharactedAction';
import ActorConversationReader from 'engine/Dialog/EngineDialogue/ActorConversationReader';
import ConversationContainer from 'engine/Dialog/EngineDialogue/ConversationContainer';
import ConversationFactory from 'engine/Dialog/Dialogue/ConversationFactory';
import Inventory from 'engine/Inventory/Inventory';
import Item from 'engine/Inventory/Item';
import HitTest from 'engine/HitTest';

let game: Game;
const girl = new Girl();
const boy = new Boy();
const inventories = new Map<Character, Inventory>([
    [girl, new Inventory()],
    [boy, new Inventory()],
]);
let controlled: DragonBonesCharacter;
let gameSocket: AbstractGameSocket;
let prisonPromise: Promise<Scene>;
let gardenPromise: Promise<Scene>;

const main = () => {
    game = new Game();

    connectGameSocket(socket => {
        gameSocket = socket;
        onGameSocketConnected();
    });

    guessControlledCharacterFromHash();
    game.viewport.follow(controlled);

    // Create scenes
    const importer = new EngineSerializer();

    prisonPromise = importer.importFile('assets/scenes/prison/prison.xml');
    gardenPromise = importer.importFile('assets/scenes/garden/garden.xml');

    // Load prison scene
    loadPrison();

    game.onClick(target => {
        controlled.moveTo(target);

        gameSocket.sendTemporized('moveControlled', 'move_character', {
            name: controlled.name,
            x: target.x,
            y: target.y,
        });
    });

    prisonPromise.then(prison => {
        // Connect doors to their room connections
        prison.getActor('door-top').on(Switch.SWITCHED, () => {
            prison
                .getRoomConnection(['cell-top', 'corridor'])
                .setOpen((prison.getActor('door-top') as Door).isOpen())
            ;
        });
        prison.getActor('door-bottom').on(Switch.SWITCHED, () => {
            prison
                .getRoomConnection(['cell-bottom', 'corridor'])
                .setOpen((prison.getActor('door-bottom') as Door).isOpen())
            ;
        });

        [
            prison.getActor('door-top'),
            prison.getActor('door-bottom'),
            prison.getActor('bed-top'),
            prison.getActor('bed-bottom'),
            boy,
            girl,
            prison.getActor('key-door-top'),
            prison.getActor('key-door-bottom'),
        ].forEach(actor => {
            actor.interactive = true;
            actor.on('mouseover', () => actor.alpha = 0.6);
            actor.on('mouseout', () => actor.alpha = 1);
            actor.on('mousedown', (event: InteractionEvent) => {
                // Prevent event propagate on viewport
                event.stopPropagation();

                controlled.moveTo(game.getPointFromEvent(event));

                if (actor instanceof Item) {
                    controlled.stackAction(new class extends AbstractCharacterAction {
                        public execute(done: () => void): void {
                            if (!HitTest.actorIsInside(controlled, actor)) {
                                console.log('Cannot loot', actor.name, ', too far from character.');
                                return;
                            }

                            console.log('loot', actor.name);
                            inventories.get(controlled).add(actor);
                            done();
                        }
                    }(controlled));
                } else {
                    controlled.stackAction(new class extends AbstractCharacterAction {
                        public execute(done: () => void): void {
                            conversations.play(this.character, actor);
                            done();
                        }
                    }(controlled));
                }
            });
        });

        document.addEventListener('keypress', event => {
            switch (event.code) {
                // Character switching
                case 'KeyS':
                    controlled = controlled === boy
                        ? girl
                        : boy
                    ;
                    game.viewport.follow(controlled);
                    break;

                // Test scene switching
                case 'KeyG':
                    if (game.getScene() === prison) {
                        loadGarden();
                    } else {
                        loadPrison();
                    }
                    break;
            }
        });
    });

    const conversations = new ConversationContainer(new ActorConversationReader());

    conversations.set(boy, girl, ConversationFactory.linear([
        {from: boy, text: 'Hello girl !'},
        {from: girl, text: 'Hello boy ! How are you ?'},
        {from: boy, text: 'Nice, but...'},
        {from: boy, action: done => {
            // Go in a cell angle, show sad face, then go back talk to girl
            const lastPos = {x: boy.position.x, y: boy.position.y};
            boy.moveTo({x: 10000, y: 10000}).asPromise()
                .then(() => {
                    alert(':(');
                    return boy.moveTo(lastPos).asPromise();
                })
                .then(done, done)
            ;
        }},
        {from: boy, text: 'I would be better out of this prison.'},
    ]));

    prisonPromise.then(prison => {
        [boy, girl].forEach(character => {
            ['door-top', 'door-bottom'].forEach(door => {
                conversations.set(character, prison.getActor(door), {
                    isChoice: false,
                    children: [
                        {
                            condition: () => !inventories.get(character).has(prison.getActor('key-' + door) as Item),
                            from: character,
                            text: 'I don\'t have the key...',
                        },
                        {
                            action: done => {
                                (prison.getActor(door) as Door).toggle();
                                done();
                            },
                        },
                    ],
                });
            });
        });

        conversations.set(girl, boy, {
            from: girl,
            text: 'Hey, boy',
            children: [
                {
                    from: girl,
                    title: 'Ask him if he is ok',
                    text: 'Are you ok ?',
                    children: [{from: boy, text: 'Yeah.'}],
                },
                {
                    from: girl,
                    text: 'Do you have the key for my door ?',
                    isChoice: false,
                    children: [
                        {
                            condition: () => inventories.get(boy).has(prison.getActor('key-door-top') as Item),
                            children: [
                                {
                                    from: boy,
                                    title: 'Give her the key',
                                    text: 'Yes, take it !',
                                    action: done => {
                                        boy.moveTo(girl).asPromise()
                                            .then(() => {
                                                const key = prison.getActor('key-door-top') as Item;
                                                inventories.get(boy).delete(key);
                                                inventories.get(girl).add(key);
                                            })
                                            .then(done, done)
                                        ;
                                    },
                                },
                                {
                                    from: boy,
                                    title: 'Keep the key',
                                    text: 'Yes, but I won\'t give you hahaha !',
                                },
                            ],
                        },
                        {
                            from: boy,
                            text: 'No, I don\'t have your key',
                        },
                    ],
                },
            ],
        });
    });

    document.body.appendChild(game.view);
    game.loader.load();
};

const loadPrison = () => {
    prisonPromise.then(prison => {
        game.loadScene(prison);
        const cellTopSpawn = Polygons.barycenter(prison.getRoom('cell-top').getArea());
        const cellBottomSpawn = Polygons.barycenter(prison.getRoom('cell-bottom').getArea());
        girl.position.set(cellTopSpawn.x, cellTopSpawn.y);
        boy.position.set(cellBottomSpawn.x, cellBottomSpawn.y);
        prison.actors.addChild(girl);
        prison.actors.addChild(boy);

        boy.listenHitTestWith(prison.getActor('scene-leaving'));
        boy.on(Character.HITTEST_ENTERS_ACTOR, (actor: Actor) => {
            if (actor === prison.getActor('scene-leaving')) {
                loadGarden();
            }
        });
    });
};

const loadGarden = () => {
    gardenPromise.then(garden => {
        game.loadScene(garden);
        const gardenSpawn = Polygons.barycenter(garden.getRoom('garden').getArea());
        girl.position.set(gardenSpawn.x, gardenSpawn.y);
        boy.position.set(gardenSpawn.x + 42, gardenSpawn.y);
        garden.actors.addChild(girl);
        garden.actors.addChild(boy);
    });
};

const connectGameSocket = (callback: (socket: AbstractGameSocket) => void): void => {
    try {
        ab.connect(
            process.env.WEBSOCKET_SERVER,
            session => {
                callback(new Websocket(session));
            },
            (code, reason, details) => {
                console.warn('Error connecting to socket, fallback to MockedSocket', code, reason, details);
                callback(new MockedSocket());
            },
        );
    } catch (err) {
        console.warn('Catched error, fallback to MockedSocket', err);
        callback(new MockedSocket());
    }
};

const onGameSocketConnected = (): void => {
    gameSocket.on('move_character', (data: {name: 'girl' | 'boy', x: number, y: number}) => {
        (data.name === 'girl' ? girl : boy).moveTo(new PixiPoint(data.x, data.y));
    });
};

const guessControlledCharacterFromHash = (): void => {
    controlled = girl;

    if ('#boy' === window.location.hash) {
        controlled = boy;
    }
};

export default main;
