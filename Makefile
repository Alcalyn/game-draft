.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-16s\033[0m %s\n", $$1, $$2}'

.PHONY: start
start: up install dev ## Install deps and start development environment

.PHONY: up
up: ## Run containers
	docker-compose up -d

.PHONY: bash
bash: ## Enter in node container
	docker-compose exec node bash

.PHONY: dev
dev: ## Start development environment
	docker-compose exec node bash -c "yarn dev"

.PHONY: lint
lint: ## Lint files
	docker-compose exec node bash -c "yarn lint"

.PHONY: test
test: ## Run tests
	docker-compose exec node bash -c "yarn test"

.PHONY: coverage
coverage: ## Run coverage report
	docker-compose exec node bash -c "yarn coverage"

build:
	docker-compose exec node bash -c "yarn build"

.PHONY: install
install: up .env ## Install deps
	docker-compose exec node bash -c "yarn install"

.env: ## Copy env file
	docker-compose exec node bash -c "cp -n .env.dist .env"

.PHONY: publish
publish: ## Publish for prod
	docker-compose exec node bash -c "./publish.sh"
	#
	# Published to: public/index.html
	#
	# Command to open locally with chromium:
	# chromium-browser public/index.html --disable-web-security --allow-file-access-from-files &
	#
